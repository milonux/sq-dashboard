import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import routes from './routes';


function RouteWithSubRoutes(route) {
  return (
    <Route
      path={route.path}
      render={props => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} />
      )}
    />
  );
}

class AppRouter extends Component {
    render() {
      return (
        <div>
          <Switch>
            {routes.map((route, i) => (
              <RouteWithSubRoutes key={i} {...route} />
            ))}
          </Switch>
        </div>
      );
    }
}

export default AppRouter;