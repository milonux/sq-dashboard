import { Home } from "../Personal/Home";
import { BidRequest } from "../Personal/BidRequest";
import { RequestProposal } from "../Personal/RequestProposal";
import { BankPersonal } from "../Personal/BankPersonal";
import { Operations } from "../Personal/Operations";
import { Finance } from "../Personal/Finance";
import { AccountSetting } from "../Personal/AccountSetting";
import { CalendarPage } from "../Personal/Calendar";

import { ManagementHome } from "../Management/Home";

const routes = [
  {
    path: "/personal-home",
    exact: true,
    component: Home
  },
  {
    path: "/bid-requests",
    exact: true,
    component: BidRequest
  },
  {
    path: "/request-for-proposal",
    exact: true,
    component: RequestProposal
  },
  {
    path: "/bank-parsonal",
    exact: true,
    component: BankPersonal
  },
  {
    path: "/calendar",
    exact: true,
    component: CalendarPage
  },
  {
    path: "/personal-operations",
    exact: true,
    component: Operations
  },
  {
    path: "/personal-finance",
    exact: true,
    component: Finance
  },
  {
    path: "/persoanl-account",
    exact: true,
    component: AccountSetting
  },
  {
    path: "/management-home",
    exact: true,
    component: ManagementHome
  }
];
export default routes;
