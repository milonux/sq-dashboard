import React, { Component } from "react";
import { Layout, Menu, Dropdown, Icon, Button, Avatar } from "antd";
import { Link } from "react-router-dom";
import AppRouter from "../../router";
import { NotificationTab } from "../components/Header";

import "./PageLayout.scss";

const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;

const userMenu = (
  <Menu style={{ minWidth: "260px" }} className="dropdown-menuuser">
    <Menu.Item style={{ padding: "0" }}>
      <div className="usermenu-header">
        <div>
          <div>
            <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
          </div>
        </div>
        <div>
          <h6>Jone Deo</h6>
          <span>jonedoe@gmail.com</span>
        </div>
      </div>
    </Menu.Item>
    <Menu.Item>
      <Link to="/persoanl-account">My Account</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="">Company Settings</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="">
        <Button type="primary" block>
          Logout
        </Button>
      </Link>
    </Menu.Item>
  </Menu>
);

const messageMenu = (
  <Menu>
    <Menu.Item>Message</Menu.Item>
  </Menu>
);

class PageLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      windowWidth: '',
      windowHeight: '',
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }


  updateDimensions() {
    var w = window,
      d = document,
      documentElement = d.documentElement,
      body = d.getElementsByTagName('body')[0],
      width = w.innerWidth || documentElement.clientWidth || body.clientWidth,
      height = w.innerHeight || documentElement.clientHeight || body.clientHeight;
    this.setState({ windowWidth: width, windowHeight: height });
    width <= 991 ? this.setState({ collapsed: true }) : this.setState({ collapsed: false })

  }
  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions);
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);
  }


  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };
  render() {
    return (
      <Layout>
        <Sider
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
          width={220}
          className="mi-sidebar"
          /* style={{
            overflow: 'auto',
            height: '100vh',
            position: 'fixed',
            left: 0,
          }} */
        >
          <div className="navbar-logo">
            <Link to="/personal-home">
              <img src="/assets/images/logo-white.png" alt="Logo" />
            </Link>
          </div>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
            <Menu.Item key="1">
              <Link to="/personal-home">
                <Icon type="dashboard" />
                <span>Dashboard</span>
              </Link>
            </Menu.Item>            
            <Menu.Item key="4">
              <Link to="/personal-operations">
                <Icon type="solution" />
                <span>Operations</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="5">
              <Link to="/personal-finance">
                <Icon type="transaction" />
                <span>Finance</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="6">
              <Link to="/personal-hr">
                <Icon type="audit" />
                <span>HR</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="7">
              <Link to="/personal-marketing">
                <Icon type="notification" />
                <span>Marketing</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="8">
              <Link to="/personal-source">
                <Icon type="project" />
                <span>Source</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="9">
              <Link to="/personal-documents">
                <Icon type="file" />
                <span>Documents</span>
              </Link>
            </Menu.Item>
            <SubMenu
              key="sub4"
              title={
                <span>
                  <Icon type="setting" />
                  <span>Settings</span>
                </span>
              }
            >
              <Menu.Item key="10">
                <Link to='/persoanl-account'>My Account</Link>
              </Menu.Item>
              <Menu.Item key="11">Company Settings</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout /* style={{ marginLeft: 220 }} */>
          <Header className="mi-header" style={{ /* position: 'fixed', zIndex: 1, width: '100%', */ padding: 0 }}>
            <div className="header-block">
              <span className="header-trigger">
                <Icon
                  className="trigger"
                  type={this.state.collapsed ? "menu-unfold" : "menu-fold"}
                  onClick={this.toggle}
                />
              </span>
              <ul className="nav-actions">
                <li>
                  <Dropdown
                    overlay={<NotificationTab />}
                    placement="bottomRight"
                    trigger={["click"]}
                    className="dropdown-notification"
                  >
                    <a className="ant-dropdown-link d-dropdown-link" href="#">
                      <Icon type="bell" />
                    </a>
                  </Dropdown>
                </li>
                <li>
                  <Dropdown
                    overlay={userMenu}
                    placement="bottomRight"
                    trigger={["click"]}
                  >
                    <a className="ant-dropdown-link d-dropdown-link" href="#">
                      <Icon type="user" />
                    </a>
                  </Dropdown>
                </li>
                <li>
                  <Dropdown
                    overlay={messageMenu}
                    placement="bottomRight"
                    trigger={["click"]}
                  >
                    <a className="ant-dropdown-link d-dropdown-link" href="#">
                      <Icon type="message" />
                    </a>
                  </Dropdown>
                </li>
              </ul>
            </div>
          </Header>
          <Content
            style={{
              padding: 40,
              minHeight: 280
            }}
          >
            <AppRouter />
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default PageLayout;
