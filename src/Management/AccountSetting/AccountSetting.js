import React, { Component } from "react";

import { Row, Col, Card, Avatar, Tabs } from "antd";

import { WrappedAccountForm } from "../components/Form";
const { TabPane } = Tabs;

class AccountSetting extends Component {
  render() {
    return (
      <div>
        <div className="page-header">
          <Row type="flex" justify="space-between">
            <Col>
              <h1 className="page-heading">My Account</h1>
            </Col>
          </Row>
        </div>
        <div id="content">
          <Row gutter={24} type="flex">
            <Col span={8} className="c-mb">
              <Card
                bordered={false}
                extra={
                  <div className="card-userinfo-header">
                    <div>
                      <div>
                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                      </div>
                    </div>
                    <div>
                      <h5 className="username">Jone Deo</h5>
                      <span>jonedoe@gmail.com</span>
                    </div>
                  </div>
                }
                className="mi-card mi-card-boxshadow mi-card-userinfo"
              >
                <div>
                  <table className="userinfo-table">
                    <tbody>
                      <tr>
                        <td>Account type</td>
                        <td>: Administrator</td>
                      </tr>
                      <tr>
                        <td>Company</td>
                        <td>: SQ</td>
                      </tr>
                      <tr>
                        <td>Title</td>
                        <td>: Germents</td>
                      </tr>
                      <tr>
                        <td>Office number</td>
                        <td>: 21534561</td>
                      </tr>
                      <tr>
                        <td>Cell number</td>
                        <td>: (151) 5235 656</td>
                      </tr>
                      <tr>
                        <td>Home number</td>
                        <td>: (161) 254-53</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td>: jondeo@gmail.com</td>
                      </tr>
                      <tr>
                        <td>Address</td>
                        <td>
                          : 4314 Centennial Farm Road
                          Wiota, IA 50274
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </Card>
            </Col>

            <Col span={16} className="c-mb">
              <Card
                bordered={false}
                className="mi-card mi-card-boxshadow mi-card-user-setting"
              >
                <Tabs defaultActiveKey="1">
                  <TabPane tab="Profile" key="1">
                    <div className="tabpane-inside-gutters">
                      <Row type="flex" justify="">
                        <Col span={16}>
                          <WrappedAccountForm />
                        </Col>
                      </Row>
                    </div>
                  </TabPane>
                  <TabPane tab="Change Password" key="2">
                    <div className="tabpane-inside-gutters"></div>
                  </TabPane>
                  <TabPane tab="Notifications" key="3">
                    <div className="tabpane-inside-gutters"></div>
                  </TabPane>
                </Tabs>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default AccountSetting;
