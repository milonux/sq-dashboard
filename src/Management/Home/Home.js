import React, { Component } from "react";

import {Link} from 'react-router-dom';


import {
  Form,
  Input,
  Select,
  Row,
  Col,
  Card,
  Modal,
  Drawer,
  Button,
  Icon
} from "antd";

/* import {
  Scatter,
  Contrast,
  ActualBar,
  DoubleLine,
  BarBasic
} from "../components/charts"; */
import Plotly from "./plotly";
import createPlotlyComponent from "react-plotly.js/factory";
import{dataChart, layoutChart} from "./DataScatter";
import{dataArea, layoutArea} from "./DataArea";
const Plot = createPlotlyComponent(Plotly);
const { Option } = Select;

const CollectionCreateForm = Form.create({ name: "form_in_modal" })(
  // eslint-disable-next-line
  class extends React.Component {
    render() {
      const { visible, onCancel, onCreate, form } = this.props;
      const { getFieldDecorator } = form;

      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 8 }
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 }
        }
      };

      return (
        <Modal
          visible={visible}
          title="Create a new collection"
          okText="Create"
          onCancel={onCancel}
          onOk={onCreate}
        >
          <Form {...formItemLayout}>
            <Form.Item label="Project ID">
              {getFieldDecorator("projectId")(<Input />)}
            </Form.Item>
            <Form.Item label="Project Name">
              {getFieldDecorator("projectName", {
                rules: [
                  {
                    required: true,
                    message: "Please input the project name of collection!"
                  }
                ]
              })(<Input />)}
            </Form.Item>
            <div className="hr-modal-inside-ng">
              <hr />
            </div>
            <p>Project Site Address</p>
            <Form.Item label="Address 1">
              {getFieldDecorator("address_1", {
                rules: [
                  {
                    required: true,
                    message: "Please input the address of collection!"
                  }
                ]
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Address 2">
              {getFieldDecorator("address 2")(<Input />)}
            </Form.Item>
            <Form.Item label="City">
              {getFieldDecorator("city")(<Input />)}
            </Form.Item>
            <Form.Item label="Description" className="mb-0">
              {getFieldDecorator("description")(<Input type="textarea" />)}
            </Form.Item>
          </Form>
        </Modal>
      );
    }
  }
);

class ManagementHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      confirmDirty: false,
      modalVisible: false,
      drawerVisible: false
    };
  }

  showModal = () => {
    this.setState({ modalVisible: true });
  };

  handleCancel = () => {
    this.setState({ modalVisible: false });
  };

  handleCreate = () => {
    const { form } = this.formRef.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      console.log("Received values of form: ", values);
      form.resetFields();
      this.setState({ modalVisible: false });
    });
  };

  saveFormRef = formRef => {
    this.formRef = formRef;
  };

  showDrawer = () => {
    this.setState({
      drawerVisible: true
    });
  };

  onClose = () => {
    this.setState({
      drawerVisible: false
    });
  };

  render() {
    return (
      <div>
        <div className="page-header">
          <Row type="flex" justify="space-between">
            <Col>
              <h1 className="page-heading">Dashboard</h1>
            </Col>
            <Col>
              <ul className="button-actions">
                <li>
                  {/* <div>
                    <Button type="primary" onClick={this.showModal}>
                      Start Project
                    </Button>
                    <Modal
                      title="New project - Project Details "
                      visible={this.state.visible}
                      onOk={this.handleOk}
                      onCancel={this.handleCancel}
                    >
                      <p>Some contents...</p>
                      <p>Some contents...</p>
                      <p>Some contents...</p>
                    </Modal>
                  </div> */}
                  <div>
                    <Button type="primary" onClick={this.showModal}>
                      Start Project
                    </Button>
                    <CollectionCreateForm
                      wrappedComponentRef={this.saveFormRef}
                      visible={this.state.modalVisible}
                      onCancel={this.handleCancel}
                      onCreate={this.handleCreate}
                    />
                  </div>
                </li>
                <li>
                  <a href="">
                    <Button type="primary" ghost>
                      Create New Proposal
                    </Button>
                  </a>
                </li>
              </ul>
            </Col>
          </Row>
        </div>

        <div id="content">
          <Row gutter={24} type="flex">
            <Col span={6} className="c-mb">
                <Link to='/bid-requests'>
                  <Card 
                    bordered={false}
                    className="mi-card mi-card-boxshadow card-statistics"
                    style={{minHeight: "100%"}}
                  >
                    <div className="card-statistics-block">
                      <div className="card-statistics-icon">
                        <Icon type="unordered-list" />
                      </div>
                      <div className="card-statistics-content">
                        <h4 className="statistics-number">51240</h4>
                        <span className="statistics-label">Change Orders</span>
                      </div>
                    </div>
                  </Card> 
                </Link>                 
            </Col>
            <Col span={6} className="c-mb">
                <Link to='/request-for-proposal'>
                  <Card 
                    bordered={false}
                    className="mi-card mi-card-boxshadow card-statistics"
                    style={{minHeight: "100%"}}
                  >
                    <div className="card-statistics-block">
                      <div className="card-statistics-icon">
                        <Icon type="border-outer" />
                      </div>
                      <div className="card-statistics-content">
                        <h4 className="statistics-number">85974</h4>
                        <span className="statistics-label">Request for Proposals</span>
                      </div>
                    </div>
                  </Card> 
                </Link>                 
            </Col>
            <Col span={6} className="c-mb">
                <Link to='/proposals'>
                  <Card 
                    bordered={false}
                    className="mi-card mi-card-boxshadow card-statistics"
                    style={{minHeight: "100%"}}
                  >
                    <div className="card-statistics-block">
                      <div className="card-statistics-icon">
                        <Icon type="solution" />
                      </div>
                      <div className="card-statistics-content">
                        <h4 className="statistics-number">35208</h4>
                        <span className="statistics-label">Proposals</span>
                      </div>
                    </div>
                  </Card> 
                </Link>                 
            </Col>
            <Col span={6} className="c-mb">
                <Link to='/projects'>
                  <Card 
                    bordered={false}
                    className="mi-card mi-card-boxshadow card-statistics"
                    style={{minHeight: "100%"}}
                  >
                    <div className="card-statistics-block">
                      <div className="card-statistics-icon">
                        <Icon type="deployment-unit" />
                      </div>
                      <div className="card-statistics-content">
                        <h4 className="statistics-number">12543</h4>
                        <span className="statistics-label">Projects</span>
                      </div>
                    </div>
                  </Card> 
                </Link>                 
            </Col>
          </Row>
          <Row gutter={24}>
            <Col span={12} className="c-mb">
              <Card
                title="Cost in BDT by category over time" 
                extra={
                  <Select defaultValue="select factory">
                    <Option value="select factory">Select factory</Option>
                  </Select>
                }
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow"
                style={{minHeight: "100%"}}
              >
                {/* <ActualBar /> */}
              

                <Plot
        data={dataChart}
        layout={layoutChart}
        style={{width:'100% !important'}}
        responsive={true}
      />
              </Card>
            </Col>
            <Col span={12} className="c-mb">
              <Card
                title="Cost in BDT by description over time" 
                extra={
                  <Select defaultValue="select factory">
                    <Option value="select factory">Select factory</Option>
                  </Select>
                }
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow"
                style={{minHeight: "100%"}}
              >

<Plot style={{width:'100% !important'}}
        data={dataArea}
        layout={layoutArea}

        responsive={true}
      />
                {/* <DoubleLine /> */}
              </Card>
            </Col>
            {/* <Col span={8} className="c-mb">
              <Card
                title="Revenue in USD by customer"
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow"
                style={{minHeight: "100%"}}
              >
                <BarBasic />
              </Card>
            </Col>
            <Col span={12} className="c-mb">
              <Card
                title="Cost in BDT by P&L heading"
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow"
                style={{minHeight: "100%"}}
              >
                <Contrast />
              </Card>
            </Col>
            <Col span={12} className="c-mb">
              <Card
                title="Cost by Activity"
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow"
                style={{minHeight: "100%"}}
              >
                <Scatter />
              </Card>
            </Col> */}
          </Row>
          <Row type="flex" gutter={24}>
            <Col span={8} className="c-mb">
              <Card
                title="Task’s"
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow"
                style={{ minHeight: "100%" }}
              >
                <div className="text-center">
                  <img
                    src="/assets/images/rocket-todo.svg"
                    alt=""
                    className="img-fluid"
                    style={{ marginBottom: "24px" }}
                  />
                  <h5>No Task’s yet.</h5>
                  <p>Create a new one by clicking on the button below.</p>
                  <div>
                    <Button type="primary" onClick={this.showDrawer}>
                      Add New Task{" "}
                    </Button>
                    <Drawer
                      title="Add New Task"
                      width={400}
                      placement="right"
                      closable={false}
                      onClose={this.onClose}
                      visible={this.state.drawerVisible}
                      className="new-task-drawer"
                    >
                      <div>
                        <Form>
                          <Form.Item label="Task Name">
                            <Input placeholder="Task Name" />
                          </Form.Item>
                          <Form.Item label="Priority">
                            <Input placeholder="Task Name" />
                          </Form.Item>
                          <Form.Item label="Type">
                            <Input placeholder="Task Name" />
                          </Form.Item>
                          <div>
                            <hr />
                          </div>
                          <p>
                            <small>Assigned To</small>
                          </p>

                          <Button type="primary" ghost>
                            Add or invite users
                          </Button>
                          <div>
                            <hr />
                          </div>
                          <Form.Item label="Proposal / Project">
                            <Select defaultValue="lucy">
                              <Option value="jack">Jack</Option>
                              <Option value="lucy">Lucy</Option>
                              <Option value="disabled" disabled>
                                Disabled
                              </Option>
                              <Option value="Yiminghe">yiminghe</Option>
                            </Select>
                          </Form.Item>
                          <Form.Item label="Due Date"></Form.Item>
                          <Form.Item label="Status">
                            <Select defaultValue="Draft">
                              <Option value="draft">Draft</Option>
                              <Option value="lucy">Lucy</Option>
                              <Option value="disabled" disabled>
                                Disabled
                              </Option>
                              <Option value="Yiminghe">yiminghe</Option>
                            </Select>
                          </Form.Item>
                          <Form.Item label="Description">
                            <Input type="textarea" />
                          </Form.Item>
                          <Form.Item>
                            <Button
                              type="primary"
                              ghost
                              style={{ marginRight: "8px" }}
                            >
                              Save
                            </Button>
                            <Button type="primary">Add Next Task</Button>
                          </Form.Item>
                        </Form>
                      </div>
                    </Drawer>
                  </div>
                </div>
              </Card>
            </Col>
            <Col span={8} className="c-mb">
              <Card
                title="My Projects"
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow"
                style={{ minHeight: "100%" }}
              >
                <div className="text-center">
                  <img
                    src="/assets/images/rocket-todo.svg"
                    alt=""
                    className="img-fluid"
                    style={{ marginBottom: "24px" }}
                  />
                  <h5>No Projects yet.</h5>
                  <p>Create a new one by clicking on the button below.</p>
                  <div>
                    <Button type="primary">Start New Project </Button>
                  </div>
                </div>
              </Card>
            </Col>
            <Col span={8} className="c-mb">
              <Card
                title="Recently Added"
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow"
                style={{ minHeight: "100%" }}
              >
                <div className="text-center">
                  <img
                    src="/assets/images/rocket-todo.svg"
                    alt=""
                    className="img-fluid"
                    style={{ marginBottom: "24px" }}
                  />
                  <h5>No Task’s yet.</h5>
                  <p>Create a new one by clicking on the button below.</p>
                </div>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default ManagementHome;
