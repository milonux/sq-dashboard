import React, { Component } from "react";
import { Row, Col, Card, Icon, Radio, Button } from "antd";

class Workspace extends Component {
  state = {
    value: null
  };

  onChange = e => {
    console.log("radio checked", e.target.value);
    this.setState({
      value: e.target.value
    });
  };

  onConfirm = async e => {
    e.preventDefault();
    await localStorage.clear();
    await localStorage.setItem("panel", `${this.state.value}`);
    console.log(localStorage.getItem("panel"));
    await this.props.history.push(`/${this.state.value}-home`);
    await window.location.reload();
  };
  render() {
    return (
      <div className="workspace-container">
        <div
          id="content"
          style={{
            width: "100%",
            maxWidth: "1000px",
            minHeight: "auto",
            margin: "auto"
          }}
        >
          <Radio.Group
            onChange={this.onChange}
            value={this.state.value}
            style={{ width: "100%", margin: "0 auto" }}
          >
            <Row type="flex" gutter={16} justify="center">
              <Col span={24}>
                <h1 className="workspace-heading">Choose Workspace Type</h1>
              </Col>
              <Col span={8} className="c-mb">
                <Card className="mi-card mi-card-boxshadow card-workspace">
                  <div>
                    <div className="workspace-icon">
                      <Icon type="user" />
                    </div>
                    <h4>Personal</h4>
                    {/* <div className="like-radio"></div> */}
                    <Radio value="personal" />
                  </div>
                </Card>
              </Col>
              <Col span={8} className="c-mb">
                <Card className="mi-card mi-card-boxshadow card-workspace">
                  <div>
                    <div className="workspace-icon">
                      <Icon type="home" />
                    </div>
                    <h4>Management</h4>
                    {/* <div className="like-radio"></div> */}
                    <Radio value="management" />
                  </div>
                </Card>
              </Col>

              <Col span={8} className="c-mb">
                <Card className="mi-card mi-card-boxshadow card-workspace">
                  <div>
                    <div className="workspace-icon">
                      <Icon type="usergroup-add" />
                    </div>
                    <h4>Board</h4>
                    {/* <div className="like-radio"></div> */}
                    <Radio value="board" />
                  </div>
                </Card>
              </Col>
            </Row>
          </Radio.Group>
          <Row type="flex" gutter={16}>
            <Col span={24} className="text-center mt-4">
              <Button
                type="primary"
                ghost
                onClick={this.onConfirm}
                disabled={this.state.value ? false : true}
              >
                Confirm
              </Button>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
export default Workspace;
