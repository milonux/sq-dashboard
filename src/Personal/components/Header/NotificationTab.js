import React, {Component} from "react";
import { Tabs, List, Avatar } from "antd";


const { TabPane } = Tabs;
const data = [
    {
      title: 'Ant Design Title 1',
    },
    {
      title: 'Ant Design Title 2',
    },
    {
      title: 'Ant Design Title 3',
    },
    {
      title: 'Ant Design Title 4',
    },
];

class NotificationTab extends Component {
    constructor(props) {
        super(props);
      }
      render() {
        return (
            <div style={{width: "340px", background: "#fff", boxShadow: "0 2px 8px rgba(0, 0, 0, 0.15)"}}>
                <div>
                    <div className="notification-dropdown-tab-heading" style={{}}>
                        <h5>Notifications</h5>
                    </div>
                    <Tabs defaultActiveKey="1" className="notification-dropdown-tab">
                        <TabPane tab="Alerts (05)" key="1">
                            <div className="tab-content-block" style={{padding: "0 0 12px"}}>
                            <List
                                itemLayout="horizontal"
                                dataSource={data}
                                renderItem={item => (
                                <List.Item>
                                    <List.Item.Meta
                                    avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                                    title={<a href="https://ant.design">{item.title}</a>}
                                    description="Ant Design, a design language for background applications, is refined by Ant UED Team"
                                    />
                                </List.Item>
                                )}
                            />
                            </div>
                        </TabPane>
                        <TabPane tab="Events (03)" key="2">
                            <div className="tab-content-block">
                                Content of Tab Pane 2
                            </div>
                        </TabPane>
                        <TabPane tab="Logs (06)" key="3">
                            <div className="tab-content-block">
                                Content of Tab Pane 2
                            </div>
                        </TabPane>
                    </Tabs>
                </div>
            </div>
        );
      }
}
export default NotificationTab;