
import { Chart, Tooltip, Axis, Legend, Bar } from "viser-react";
import * as React from "react";


class ChartHistoryGroupColumn extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    
    return (
      <Chart forceFit height={400} data={this.props.chartData}>
        <Tooltip />
        <Axis />
        <Legend />
        <Bar
          position="keySelect*average"
          color="name"
          adjust={[{ type: "dodge", marginRatio: 1 / 32 }]}
        />
      </Chart>
    );
  }
}
export default ChartHistoryGroupColumn;
