import { Chart, Tooltip, Axis, Legend, Coord, Guide, Pie } from 'viser-react';
import * as React from 'react';
const DataSet = require('@antv/data-set');

const sourceData = [
  { item: 'Groceries', count: 40 },
  { item: 'Shopping', count: 21 },
  { item: 'Utility Bill', count: 17 },
  { item: 'Food', count: 13 },
  { item: 'Ohters', count: 9 }
];

const scale = [{
  dataKey: 'percent',
  min: 0,
  formatter: '.0%',
}];

const dv = new DataSet.View().source(sourceData);
dv.transform({
  type: 'percent',
  field: 'count',
  dimension: 'item',
  as: 'percent'
});
const data = dv.rows;

class ChartBreakdownDonut extends React.Component {
  render() {
    return (
      <Chart width={400} height={400} data={data} scale={scale}>
        <Tooltip showTitle={false} />
        <Axis />
        <Legend dataKey="item" />
        <Coord type="theta" radius={0.75} innerRadius={0.6} />
        <Pie position="percent" color="item" style={{ stroke: '#fff', lineWidth: 1 }}
          label={['percent', {
            formatter: (val, item) => {
              return item.point.item + ': ' + val;
            }
          }]}
        />
      </Chart>
    );
  }
}
export default ChartBreakdownDonut;






