import React, { Component } from "react";

import {
    Form,
    Input,
    Divider,
    Tooltip,
    Icon,
    Cascader,
    Select,
    Row,
    Col,
    Card,
    Modal,
    Drawer,
    Checkbox,
    Button,
    Radio
} from "antd";

class FormAccountSetting extends Component {
    render () {
        const formItemLayout = {
            labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
            },
            wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
            xs: {
                span: 24,
                offset: 0,
            },
            sm: {
                span: 16,
                offset: 8,
            },
            },
        };
        const { getFieldDecorator } = this.props.form;
        return(
            <Form {...formItemLayout}>
                
                <Row>
                    <Col span={16} offset={8}>
                        <h6 className="mb-3">Contact Information</h6>
                    </Col>
                </Row>
                <Form.Item label="First Name">
                    {getFieldDecorator('fname')(<Input />)}
                </Form.Item>
                <Form.Item label="Last Name">
                    {getFieldDecorator('lname')(<Input />)}
                </Form.Item>
                <Form.Item label="Office Phone">
                    {getFieldDecorator('officePhone')(<Input />)}
                </Form.Item>
                <Form.Item label="Cell Phone">
                    {getFieldDecorator('cellPhone')(<Input />)}
                </Form.Item>
                <Form.Item label="Home Phone">
                    {getFieldDecorator('homePhone')(<Input />)}
                </Form.Item>

                <Row>
                    <Col span={16} offset={8}>
                        <h6 className="mb-3">Address</h6>
                    </Col>
                </Row>
                <Form.Item label="Address 1">
                    {getFieldDecorator('address_1')(<Input />)}
                </Form.Item>
                <Form.Item label="Address 2">
                    {getFieldDecorator('address_2')(<Input />)}
                </Form.Item>
                <Form.Item label="City">
                    {getFieldDecorator('city')(<Input />)}
                </Form.Item>
                <Form.Item label="State">
                    {getFieldDecorator('state')(<Input />)}
                </Form.Item>
                <Form.Item label="Zip Code">
                    {getFieldDecorator('zipCode')(<Input />)}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit">
                        Update
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}
const WrappedAccountForm = Form.create()(FormAccountSetting);
export default WrappedAccountForm;