import React, {Component} from "react";

import {
    Row,
    Col,
    Card,
    Calendar, Badge
} from "antd";


function getListData(value) {
    let listData;
    switch (value.date()) {
      case 8:
        listData = [
          { type: 'warning', content: 'This is warning event.' },
          { type: 'success', content: 'This is usual event.' },
        ];
        break;
      case 10:
        listData = [
          { type: 'warning', content: 'This is warning event.' },
          { type: 'success', content: 'This is usual event.' },
          { type: 'error', content: 'This is error event.' },
        ];
        break;
      case 15:
        listData = [
          { type: 'warning', content: 'This is warning event' },
          { type: 'success', content: 'This is very long usual event。。....' },
          { type: 'error', content: 'This is error event 1.' },
          { type: 'error', content: 'This is error event 2.' },
          { type: 'error', content: 'This is error event 3.' },
          { type: 'error', content: 'This is error event 4.' },
        ];
        break;
      default:
    }
    return listData || [];
  }
  
  function dateCellRender(value) {
    const listData = getListData(value);
    return (
      <ul className="events">
        {listData.map(item => (
          <li key={item.content}>
            <Badge status={item.type} text={item.content} />
          </li>
        ))}
      </ul>
    );
  }
  
  function getMonthData(value) {
    if (value.month() === 8) {
      return 1394;
    }
  }
  
  function monthCellRender(value) {
    const num = getMonthData(value);
    return num ? (
      <div className="notes-month">
        <section>{num}</section>
        <span>Backlog number</span>
      </div>
    ) : null;
  }

class CalendarPage extends Component {
    render () {
        return (
            <div>
                <div className="page-header">
                    <Row type="flex" justify="space-between">
                        <Col>
                            <h1 className="page-heading">Calendar</h1>
                        </Col>
                    </Row>
                </div>
                <div id="content">
                    <Row type="flex" gutter={24}>
                        <Col md={24}>
                            <Card 
                              style={{minHeight: '100%'}}
                              bordered={false}
                              className="mi-card mi-card-header-borderless mi-card-boxshadow"
                            >
                                <div>
                                    <Calendar dateCellRender={dateCellRender} monthCellRender={monthCellRender} />
                                </div>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}
export default CalendarPage;