import React, { Component } from "react";
import { Row, Col, Card, Input, Table, Button, Tag, Divider } from "antd";

const { Search } = Input;

const columns = [
    {
      title: 'Order Number',
      dataIndex: 'oNumber',
      key: 'oNumber',
    },
    {
      title: 'Confirmation Date',
      dataIndex: 'confDate',
      key: 'confDate',
    },
    {
      title: 'Customer',
      dataIndex: 'customer',
      key: 'customer',
    },
    {
      title: 'Salesperson',
      dataIndex: 'salePer',
      key: 'salePer',
    },
    {
      title: 'Total',
      dataIndex: 'total',
      key: 'total',
    },
    {
      title: 'Invoice Status',
      key: 'invoStatus',
      dataIndex: 'tags',
      render: tags => (
        <span>
          {tags.map(tag => {
            let color = tag.length > 5 ? 'geekblue' : 'green';
            if (tag === 'loser') {
              color = 'volcano';
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </span>
      ),
    },
    {
      title: 'Action',
      key: 'action',
      render: (text, record) => (
        <span>
          <a>Edit {record.name}</a>
          <Divider type="vertical" />
          <a>Delete</a>
        </span>
      ),
    },
  ];
  
  const data = [
    {
      key: '1',
      oNumber: 'DVO524',
      confDate: '05/07/2019',
      customer: 'Jone Deo',
      salePer: 'SQBL',
      total: '$26300',
      tags: ['Success'],
    },
    {
      key: '2',
      oNumber: 'DVO524',
      confDate: '05/07/2019',
      customer: 'Jone Deo',
      salePer: 'SQBL',
      total: '$26300',
      tags: ['Success'],
    },
    {
      key: '3',
      oNumber: 'DVO524',
      confDate: '05/07/2019',
      customer: 'Jone Deo',
      salePer: 'SQBL',
      total: '$26300',
      tags: ['Success'],
    },
    {
      key: '4',
      oNumber: 'DVO524',
      confDate: '05/07/2019',
      customer: 'Jone Deo',
      salePer: 'SQBL',
      total: '$26300',
      tags: ['Success'],
    },
    {
      key: '5',
      oNumber: 'DVO524',
      confDate: '05/07/2019',
      customer: 'Jone Deo',
      salePer: 'SQBL',
      total: '$26300',
      tags: ['Success'],
    },
    {
      key: '6',
      oNumber: 'DVO524',
      confDate: '05/07/2019',
      customer: 'Jone Deo',
      salePer: 'SQBL',
      total: '$26300',
      tags: ['Success'],
    },
    {
      key: '7',
      oNumber: 'DVO524',
      confDate: '05/07/2019',
      customer: 'Jone Deo',
      salePer: 'SQBL',
      total: '$26300',
      tags: ['Success'],
    },
    {
      key: '8',
      oNumber: 'DVO524',
      confDate: '05/07/2019',
      customer: 'Jone Deo',
      salePer: 'SQBL',
      total: '$26300',
      tags: ['Success'],
    },
    {
      key: '9',
      oNumber: 'DVO524',
      confDate: '05/07/2019',
      customer: 'Jone Deo',
      salePer: 'SQBL',
      total: '$26300',
      tags: ['Success'],
    },
    {
      key: '10',
      oNumber: 'DVO524',
      confDate: '05/07/2019',
      customer: 'Jone Deo',
      salePer: 'SQBL',
      total: '$26300',
      tags: ['Success'],
    },
  ];

class RequestProposal extends Component {
    render () {
        return (
            <div>
                <div className="page-header">
                    <Row type="flex" justify="space-between">
                        <Col>
                            <h1 className="page-heading">Request for Proposal</h1>
                        </Col>
                        <Col>
                            <ul className="button-actions">
                                <li>
                                    <Search
                                        placeholder="Search..."
                                        style={{ width: 300 }}
                                    />
                                </li>
                            </ul>
                        </Col>
                    </Row>
                </div>
                <div id="content">
                    <Card
                        bordered={false}
                        className="mi-card mi-card-boxshadow card-filled-table"
                    >
                        <Table columns={columns} dataSource={data} /> 
                    </Card>
                    <div className="button-iocn-foo-fixed-xl">
                      <Button type="primary" shape="circle" size="large" icon="plus"  />
                    </div>
                </div>
            </div>
        );
    }
}
export default RequestProposal;