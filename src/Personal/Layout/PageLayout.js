import React, { Component, useState, useEffect }  from "react";
import { Layout, Menu, Dropdown, Icon, Button, Avatar } from "antd";
import { Link } from "react-router-dom";
import AppRouter from "../../router";
import { NotificationTab } from "../components/Header";

import "./PageLayout.scss";
import { isAbsolute } from "path";

const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;
const forMobileStyle={
  marginLeft:0,
  position:'absolute'

}
const forTabStyle={
  marginLeft:80,
  

}
const forDesktopStyle={
  marginLeft:220,
  

}


const userMenu = (
  <Menu style={{ minWidth: "260px" }} className="dropdown-menuuser">
    <Menu.Item style={{ padding: "0" }}>
      <div className="usermenu-header">
        <div>
          <div>
            <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
          </div>
        </div>
        <div>
          <h6>Jone Deo</h6>
          <span>jonedoe@gmail.com</span>
        </div>
      </div>
    </Menu.Item>
    <Menu.Item>
      <Link to="/persoanl-account">My Account</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="">Company Settings</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="">
        <Button type="primary" block>
          Logout
        </Button>
      </Link>
    </Menu.Item>
  </Menu>
);

const messageMenu = (
  <Menu>
    <Menu.Item>Message</Menu.Item>
  </Menu>
);

class PageLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      windowWidth:window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth,
      windowHeight:  window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeigh,
    }
    this.updateDimensions = this.updateDimensions.bind(this);
  }


  updateDimensions() {
    var w = window,
      d = document,
      documentElement = d.documentElement,
      body = d.getElementsByTagName('body')[0],
      width = w.innerWidth || documentElement.clientWidth || body.clientWidth,
      height = w.innerHeight || documentElement.clientHeight || body.clientHeight;
      console.log("Width : "+width);
    this.setState({ windowWidth: width, windowHeight: height });
    width <= 991 ? this.setState({ collapsed: true }) : this.setState({ collapsed: false })

  }
  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions);
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions);
  }


  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };
  render() {
    return (
      <Layout>
        <Sider
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
          width={220}
          className="mi-sidebar"
          /* style={{
            overflow: 'auto',
            height: '100vh',
            position: 'fixed',
            left: 0,
          }} */
        >
          <div className="navbar-logo">
            <Link to="/personal-home">
              <img src="/assets/images/logo-white.png" alt="Logo" />
            </Link>
          </div>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
            <Menu.Item key="1">
              <Link to="/personal-home">
                <Icon type="dashboard" />
                <span>Dashboard</span>
              </Link>
            </Menu.Item>
            <SubMenu
              key="sub5"
              title={
                <span>
                  <Icon type="bank" />
                  <span>Bank</span>
                </span>
              }
            >
              <Menu.Item key="15">
                <Link to='/bank-parsonal'>Personal</Link>
              </Menu.Item>
              <Menu.Item key="18">
                <Link to='/bank-corporate'>Corporate</Link>
              </Menu.Item>
            </SubMenu>
            <Menu.Item key="2">
              <Link to="/news">
                <Icon type="pic-left" />
                <span>News</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="16">
              <Link to="/weather">
                <Icon type="cloud" />
                <span>Weather</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="17">
              <Link to="/calendar">
                <Icon type="calendar" />
                <span>Calendar</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="7">
              <Link to="/personal-wallet">
                <Icon type="wallet" />
                <span>Wallet</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="8">
              <Link to="/personal-source">
                <Icon type="project" />
                <span>Source</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="9">
              <Link to="/personal-documents">
                <Icon type="file" />
                <span>Documents</span>
              </Link>
            </Menu.Item>
            <SubMenu
              key="sub4"
              title={
                <span>
                  <Icon type="setting" />
                  <span>Settings</span>
                </span>
              }
            >
              <Menu.Item key="10">
                <Link to='/persoanl-account'>My Account</Link>
              </Menu.Item>
              <Menu.Item key="11">Company Settings</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout /* style={{ marginLeft: 220 }} */>
          <Header className="mi-header" style={{ /* position: 'fixed', zIndex: 1, width: '100%', */ padding: 0 }}>
            <div className="header-block">
              <span className="header-trigger">
                <Icon
                  className="trigger"
                  type={this.state.collapsed ? "menu-unfold" : "menu-fold"}
                  onClick={this.toggle}
                />
              </span>
              <ul className="nav-actions">
                <li>
                  <Dropdown
                    overlay={<NotificationTab />}
                    placement="bottomRight"
                    trigger={["click"]}
                    className="dropdown-notification"
                  >
                    <a className="ant-dropdown-link d-dropdown-link" href="#">
                      <Icon type="bell" />
                    </a>
                  </Dropdown>
                </li>
                <li>
                  <Dropdown
                    overlay={userMenu}
                    placement="bottomRight"
                    trigger={["click"]}
                  >
                    <a className="ant-dropdown-link d-dropdown-link" href="#">
                      <Icon type="user" />
                    </a>
                  </Dropdown>
                </li>
                <li>
                  <Dropdown
                    overlay={messageMenu}
                    placement="bottomRight"
                    trigger={["click"]}
                  >
                    <a className="ant-dropdown-link d-dropdown-link" href="#">
                      <Icon type="message" />
                    </a>
                  </Dropdown>
                </li>
              </ul>
            </div>
          </Header>
          <Content
            style={{
              padding: 40,
              minHeight: 280
            }}
          >
            <AppRouter />
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default PageLayout;
