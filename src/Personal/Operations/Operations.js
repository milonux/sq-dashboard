import React, {Component} from "react";

import {
    Row,
    Col,
    Card,
    Table, 
    Divider, 
    Tag,
    Input
} from "antd";

const { Search } = Input;


const columns = [
  {
    title: 'Order Number',
    dataIndex: 'oNumber',
    key: 'oNumber',
  },
  {
    title: 'Confirmation Date',
    dataIndex: 'confDate',
    key: 'confDate',
  },
  {
    title: 'Customer',
    dataIndex: 'customer',
    key: 'customer',
  },
  {
    title: 'Salesperson',
    dataIndex: 'salePer',
    key: 'salePer',
  },
  {
    title: 'Total',
    dataIndex: 'total',
    key: 'total',
  },
  {
    title: 'Invoice Status',
    key: 'invoStatus',
    dataIndex: 'tags',
    render: tags => (
      <span>
        {tags.map(tag => {
          let color = tag.length > 5 ? 'geekblue' : 'green';
          if (tag === 'loser') {
            color = 'volcano';
          }
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </span>
    ),
  },
  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
      <span>
        <a>Edit {record.name}</a>
        <Divider type="vertical" />
        <a>Delete</a>
      </span>
    ),
  },
];

const data = [
  {
    key: '1',
    oNumber: 'DVO524',
    confDate: '05/07/2019',
    customer: 'Jone Deo',
    salePer: 'SQBL',
    total: '$26300',
    tags: ['Success'],
  },
  {
    key: '1',
    oNumber: 'DVO524',
    confDate: '05/07/2019',
    customer: 'Jone Deo',
    salePer: 'SQBL',
    total: '$26300',
    tags: ['Success'],
  },
  {
    key: '1',
    oNumber: 'DVO524',
    confDate: '05/07/2019',
    customer: 'Jone Deo',
    salePer: 'SQBL',
    total: '$26300',
    tags: ['Success'],
  },
  {
    key: '1',
    oNumber: 'DVO524',
    confDate: '05/07/2019',
    customer: 'Jone Deo',
    salePer: 'SQBL',
    total: '$26300',
    tags: ['Success'],
  },
  {
    key: '1',
    oNumber: 'DVO524',
    confDate: '05/07/2019',
    customer: 'Jone Deo',
    salePer: 'SQBL',
    total: '$26300',
    tags: ['Success'],
  },
  {
    key: '1',
    oNumber: 'DVO524',
    confDate: '05/07/2019',
    customer: 'Jone Deo',
    salePer: 'SQBL',
    total: '$26300',
    tags: ['Success'],
  },
  {
    key: '1',
    oNumber: 'DVO524',
    confDate: '05/07/2019',
    customer: 'Jone Deo',
    salePer: 'SQBL',
    total: '$26300',
    tags: ['Success'],
  },
  {
    key: '1',
    oNumber: 'DVO524',
    confDate: '05/07/2019',
    customer: 'Jone Deo',
    salePer: 'SQBL',
    total: '$26300',
    tags: ['Success'],
  },
  {
    key: '1',
    oNumber: 'DVO524',
    confDate: '05/07/2019',
    customer: 'Jone Deo',
    salePer: 'SQBL',
    total: '$26300',
    tags: ['Success'],
  },
  {
    key: '1',
    oNumber: 'DVO524',
    confDate: '05/07/2019',
    customer: 'Jone Deo',
    salePer: 'SQBL',
    total: '$26300',
    tags: ['Success'],
  },
];

class Operations extends Component {
    render () {
        return(
            <div>
                <div className="page-header">
                    <Row type="flex" justify="space-between">
                        <Col>
                            <h1 className="page-heading">Operations</h1>
                        </Col>
                        <Col>
                            <ul className="button-actions">
                                {/* <li>
                                    <a href="">
                                        <Button type="primary" ghost>
                                        Create New Proposal
                                        </Button>
                                    </a>
                                </li> */}
                            </ul>
                        </Col>
                    </Row>
                </div>
                <div id="content">
                    <Row type="flex" gutter={24}>
                        <Col span={24} className="c-mb">
                            <Card
                                //title="Sales"
                                bordered={false}
                                className="mi-card mi-card-boxshadow"
                            >
                                <div>
                                    <Row type="flex" justify="end" className="table-filters">
                                        <Col className="mb-3">
                                            <Search
                                                placeholder="input search text"
                                                onSearch={value => console.log(value)}
                                                style={{ width: 300 }}
                                            />
                                        </Col>
                                    </Row>
                                    <div>
                                        <Table columns={columns} dataSource={data} />
                                    </div>
                                </div>
                            </Card> 
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}
export default Operations;