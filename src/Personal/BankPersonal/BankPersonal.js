import React, { Component } from "react";

import { Row, Col, Card, Button, Icon, Progress } from "antd";

import ChartHistoryGroupColumn from "../components/charts/ChartHistoryGroupColumn";
import ChartBreakdownDonut from "../components/charts/ChartBreakdownDonut";
const DataSet = require("@antv/data-set");
const sourceDataDaily = [
  {
    name: "Recieved",
    "1AM": 18.9,
    "2AM": 28.8,
    "3AM": 39.3,
    "4AM": 75.4,
    "5AM": 26.4,
    "6AM": 11.4,
    "7AM": 35.4,
    "8AM": 50.4,
    "9AM": 62.4,
    "10AM": 25.4,
    "11AM": 67.4,
    "12PM": 98.4,
    "1PM": 60.9,
    "2PM": 45.8,
    "3PM": 33.3,
    "4PM": 65.4,
    "5PM": 89.4,
    "6PM": 75.4,
    "7PM": 19.4,
    "8PM": 12.4,
    "9PM": 36.4,
    "10PM": 20.4,
    "11PM": 19.4,
    "12PM": 95.4,
  },
  {
    name: "Sent",
    "1AM": 90.9,
    "2AM": 71.8,
    "3AM": 99.3,
    "4AM": 25.4,
    "5AM": 65.4,
    "6AM": 85.4,
    "7AM": 47.4,
    "8AM": 36.4,
    "9AM": 22.4,
    "10AM": 33.4,
    "11AM": 88.4,
    "12PM": 44.4,
    "1PM": 66.9,
    "2PM": 67.8,
    "3PM": 20.3,
    "4PM": 19.4,
    "5PM": 37.4,
    "6PM": 86.4,
    "7PM": 54.4,
    "8PM": 77.4,
    "9PM": 44.4,
    "10PM": 25.4,
    "11PM": 37.4,
    "12PM": 46.4,
  }
];
const sourceDataWeekly = [
  {
    name: "Recieved",
    "Sat.": 98.9,
    "Sun.": 28.8,
    "Mon.": 69.3,
    "Tue.": 81.4,
    "Wed.": 47,
    "Thu.": 20.3,
    "Fri.": 24
  },
  {
    name: "Sent",
    "Sat.": 18.4,
    "Sun.": 33.2,
    "Mon.": 54.5,
    "Tue.": 99.7,
    "Wed.": 52.6,
    "Thu.": 45.5,
    "Fri.": 67.4
  }
];
const sourceDataMonthly = [
  {
    name: "Recieved",
    "Jan.": 69.9,
    "Feb.": 25.8,
    "Mar.": 70.3,
    "Apr.": 67.4,
    "May.": 65,
    "Jun.": 45.3,
    "Jul.": 82,
    "Sep.": 72.6,
    "Oct.": 65.6,
    "Nov.": 45.6,
    "Dec.": 90.6
  },
  {
    name: "Sent",
    "Jan.": 48.9,
    "Feb.": 38.8,
    "Mar.": 40.3,
    "Apr.": 31.4,
    "May.": 47,
    "Jun.": 40.3,
    "Jul.": 24,
    "Sep.": 38.6,
    "Oct.": 38.6,
    "Nov.": 38.6,
    "Dec.": 38.6
  }
];
class BankPersonal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      chartData: [],
      selectedKey: "daily"
    };
    this.onChangeChart = this.onChangeChart.bind(this);
  }
  componentDidMount() {
    const dv = new DataSet.View().source(sourceDataDaily);
    dv.transform({
      type: "fold",
      fields: ["1AM","2AM","3AM","4AM","5AM","6AM","7AM","8AM","9AM","10AM","11AM","12PM","1PM","2PM","3PM","4PM","5PM","6PM","7PM","8PM","9PM","10PM","11PM","12PM"],
      key: "keySelect",
      value: "average"
    });
    const data = dv.rows;
    this.setState({ chartData: data, selectedKey: "daily" });
  }

  onChangeChart(value) {
    if (value === "daily") {
      const dv = new DataSet.View().source(sourceDataDaily);
      dv.transform({
        type: "fold",
        fields: ["1AM","2AM","3AM","4AM","5AM","6AM","7AM","8AM","9AM","10AM","11AM","12PM","1PM","2PM","3PM","4PM","5PM","6PM","7PM","8PM","9PM","10PM","11PM","12PM"],
        key: "keySelect",
        value: "average"
      });
      const data = dv.rows;
      this.setState({ chartData: data, selectedKey: value });
    } else if (value === "weekly") {
      const dv = new DataSet.View().source(sourceDataWeekly);
      dv.transform({
        type: "fold",
        fields: ["Sat.","Sun.","Mon.","Tue.","Wed.","Thu.","Fri."],
        key: "keySelect",
        value: "average"
      });
      const data = dv.rows;
      this.setState({ chartData: data, selectedKey: value });
    } else if (value === "monthly") {
      

      const dv = new DataSet.View().source(sourceDataMonthly);
      dv.transform({
        type: "fold",
        fields: ["Jan.","Feb.","Mar.","Apr.","May.","Jun.","Jul.","Sep.","Oct.","Nov.","Dec."],
        key: "keySelect",
        value: "average"
      });
      const data = dv.rows;
      this.setState({ chartData: data, selectedKey: value });
    }
    console.log("Selected :" + value);
    console.log(this.state.chartData);
  }

  render() {
    return (
      <div>
        <div className="page-header">
          <Row type="flex" justify="space-between">
            <Col>
              <h1 className="page-heading">Bank Personal</h1>
            </Col>
          </Row>
        </div>
        <div id="content">
          <Row type="flex" gutter={24}>
            <Col xs={24} sm={24} md={12} lg={12} xl={6} className="c-mb">
              <Card
                style={{ minHeight: "100%" }}
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow card-statistics-deposit"
              >
                <div className="card-statistics-block">
                  <h2 className="statistics-number">
                    <span className="price-sign">$</span>
                    25,523
                  </h2>
                  <div className="statistics-label">
                    <span>Deposits:</span> <span>$15,000</span>
                  </div>
                  <div className="statistics-deposit-suffix">+5.2% ($653)</div>
                  <div className="my-5">
                    <Button type="primary" shape="round" size="large" block>
                      Add Funds
                    </Button>
                  </div>
                  <div className="card-statistics-deposit-footer">
                    <div className="card-statistics-deposit-footer-block">
                      <ul className="me">
                        <li>
                          <div className="statistics-label">
                            <span>Goal:</span> <span>$55,000</span>
                          </div>
                          <Progress
                            strokeColor={{
                              from: "#108ee9",
                              to: "#87d068"
                            }}
                            percent={77}
                          />
                        </li>
                        <li>
                          <div className="statistics-label">
                            <span>Duration:</span> <span>4year</span>
                          </div>
                          <Progress
                            strokeColor={{
                              from: "#108ee9",
                              to: "#92278f"
                            }}
                            percent={57}
                          />
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </Card>
            </Col>
            <Col xs={24} sm={24} md={12} lg={12} xl={11} className="c-mb">
              <Card
                style={{ minHeight: "100%" }}
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow card-statistics-accounts"
              >
                <div>
                  <div className="ant-card-head-title p-0 mb-1">
                    Bank Accounts
                  </div>
                  <ul>
                    <li>
                      <div className="card-b-float">
                        <div className="card-b-float-icon">
                          <Icon type="bank" />
                        </div>
                        <div className="card-b-float-block">
                          <div className="card-b-float-block-cell">
                            <h4 className="card-b-float-title">Checking</h4>
                            <span>...5263</span>
                          </div>
                          <div className="card-b-float-block-cell">
                            <h5 className="card-b-float-title">$523.00</h5>
                            <span>Balance</span>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className="card-b-float">
                        <div className="card-b-float-icon">
                          <Icon type="bank" />
                        </div>
                        <div className="card-b-float-block">
                          <div className="card-b-float-block-cell">
                            <h4 className="card-b-float-title">Savings</h4>
                            <span>...6879</span>
                          </div>
                          <div className="card-b-float-block-cell">
                            <h5 className="card-b-float-title">$895.00</h5>
                            <span>Balance</span>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>

                  <div className="ant-card-head-title p-0 mt-3 mb-1">
                    Credit Cards
                  </div>
                  <ul>
                    <li>
                      <div className="card-b-float">
                        <div className="card-b-float-icon">
                          <Icon type="credit-card" />
                        </div>
                        <div className="card-b-float-block">
                          <div className="card-b-float-block-cell">
                            <h4 className="card-b-float-title">Visa</h4>
                            <span>...9263</span>
                          </div>
                          <div className="card-b-float-block-cell">
                            <h5 className="card-b-float-title">$730.00</h5>
                            <span>Balance</span>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className="card-b-float">
                        <div className="card-b-float-icon">
                          <Icon type="credit-card" />
                        </div>
                        <div className="card-b-float-block">
                          <div className="card-b-float-block-cell">
                            <h4 className="card-b-float-title">Mastercard</h4>
                            <span>...4279</span>
                          </div>
                          <div className="card-b-float-block-cell">
                            <h5 className="card-b-float-title">$495.00</h5>
                            <span>Balance</span>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </Card>
            </Col>
            <Col xs={24} sm={24} md={12} lg={12} xl={7} className="c-mb">
              <Card
                title="Recent Transactions"
                extra={<a href="#">View all</a>}
                style={{ minHeight: "100%" }}
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow"
              >
                <ul className="transaction-lists">
                  <li>
                    <div className="transaction-item">
                      <span>Starbucks</span>
                      <span>$5.10</span>
                    </div>
                  </li>
                  <li>
                    <div className="transaction-item">
                      <span>Whole Foods</span>
                      <span>$20.50</span>
                    </div>
                  </li>
                  <li>
                    <div className="transaction-item">
                      <span>Universal Studios</span>
                      <span>$16.25</span>
                    </div>
                  </li>
                  <li>
                    <div className="transaction-item">
                      <span>Eastsa</span>
                      <span>$3.15</span>
                    </div>
                  </li>
                  <li>
                    <div className="transaction-item">
                      <span>Peet's Coffee</span>
                      <span>$11.36</span>
                    </div>
                  </li>
                  <li>
                    <div className="transaction-item">
                      <span>Target</span>
                      <span>$30.22</span>
                    </div>
                  </li>
                </ul>
              </Card>
            </Col>
            <Col xs={24} sm={24} md={{span:24, order:5}} lg={24} xl={{span: 16, order: 4}} className="c-mb">
              <Card
                title="History"
                style={{ minHeight: "100%" }}
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow"
              >
                <div>
                  <ul className="history-filters">
                    <li
                      className={
                        this.state.selectedKey === "daily" ? "active" : null
                      }
                    >
                      <span onClick={() => this.onChangeChart("daily")}>
                        Daily
                      </span>
                    </li>
                    <li
                      className={
                        this.state.selectedKey === "weekly" ? "active" : null
                      }
                    >
                      <span onClick={() => this.onChangeChart("weekly")}>
                        Weekly
                      </span>
                    </li>
                    <li
                      className={
                        this.state.selectedKey === "monthly" ? "active" : null
                      }
                    >
                      <span onClick={() => this.onChangeChart("monthly")}>
                        Monthly
                      </span>
                    </li>
                  </ul>
                  <ChartHistoryGroupColumn chartData={this.state.chartData} />
                </div>
              </Card>
            </Col>
            <Col xs={24} sm={24} md={{span: 12, order: 4}} lg={12} xl={{span:8, order: 5}} className="c-mb">
              <Card
                title="Breakdown"
                style={{ minHeight: "100%" }}
                bordered={false}
                className="mi-card mi-card-header-borderless mi-card-boxshadow"
              >
                <div>
                  <ChartBreakdownDonut />
                </div>
              </Card>
            </Col>
          </Row>
          <Row type="flex" gutter={24}>
            
          </Row>
        </div>
      </div>
    );
  }
}
export default BankPersonal;
