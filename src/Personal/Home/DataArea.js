
var trace1, trace2, trace3, trace4, trace5, trace6, trace7, trace8, trace9, trace10, trace11, trace12, trace13, trace14, trace15, trace16, trace17, trace18, trace19, trace20, trace21, trace22, trace23, trace24, trace25, trace26, trace27, trace28, trace29, trace30, trace31, trace32, trace33, trace34, trace35, trace36, trace37, trace38, trace39, trace40, trace41, trace42, trace43, trace44, trace45, trace46, trace47, trace48, trace49, trace50, trace51, trace52, trace53, trace54, trace55, trace56, trace57, trace58, trace59, trace60, trace61, trace62, trace63, trace64, trace65, trace66, trace67, trace68, trace69, trace70, trace71, trace72, trace73, trace74, trace75, trace76, trace77, trace78, trace79, trace80, trace81, trace82, trace83, trace84, trace85, trace86, trace87, trace88, trace89, trace90, trace91, trace92, trace93, trace94, trace95, trace96, trace97, trace98, trace99, trace100, trace101, trace102, trace103, trace104, trace105, trace106, trace107, trace108, trace109, trace110, trace111, trace112, trace113, trace114, trace115, trace116;
trace1 = {
    mode: 'markers', 
    name: 'Description=Overtime - Extra', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:c767bc', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:4d42f5', 
    y: [22932.549977827, 15288.3666518847, 22083.1962749446, 22932.549977827, 21233.8425720621, 22083.1962749446, 21233.8425720621, 21233.8425720621, 21233.8425720621, 20384.4888691796, 14439.0129490022, 22083.1962749446, 1608960.74423044, 0, 1072640.49615363, 0, 1549369.60555524, 0, 1608960.74423044, 0, 1489778.46688004, 0, 1549369.60555524, 0, 1489778.46688004, 0, 1489778.46688004, 0, 1489778.46688004, 0, 1430187.32820484, 0, 1013049.35747843, 0, 1549369.60555524, 0, 720202.382362438, 0, 0, 480134.921574959, 0, 0, 693528.220052718, 0, 0, 720202.382362438, 0, 0, 666854.057742998, 0, 0, 693528.220052718, 0, 0, 666854.057742998, 0, 0, 666854.057742998, 0, 0, 666854.057742998, 0, 0, 640179.895433278, 0, 0, 453460.759265239, 0, 0, 693528.220052718, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#636efa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Overtime - Extra', 
    hovertextsrc: 'sumon23:120:16e39f', 
    hovertext: ['Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Overtime - Extra<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace2 = {
    mode: 'markers', 
    name: 'Description=Wages', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:19cee5', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:150852', 
    y: [39334340.9172113, 39334340.9172113, 39334340.9172113, 39334340.9172113, 39334340.9172113, 39334340.9172113, 39334340.9172113, 39334340.9172113, 39334340.9172113, 39334340.9172113, 39334340.9172113, 39334340.9172113, 17581774.5821272, 17581774.5821272, 17581774.5821272, 17581774.5821272, 17581774.5821272, 17581774.5821272, 17581774.5821272, 17581774.5821272, 17581774.5821272, 17581774.5821272, 17581774.5821272, 17581774.5821272, 30135735.9214807, 25298938.4880667, 29724412.3863718, 30267446.4541684, 26543650.0134904, 25724358.4067107, 24124086.6958053, 26553358.1757382, 28367214.9389574, 28641569.2691702, 26079482.1375705, 29353877.6095957, 55835950.7148664, 56057968.826279, 57275330.7882282, 57275330.7882282, 57275330.7882282, 57275330.7882282, 57275330.7882282, 30762800.0082777, 57275330.7882282, 63043415.0213892, 63043415.0213892, 63043415.0213892], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#EF553B', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Wages', 
    hovertextsrc: 'sumon23:120:e14a99', 
    hovertext: ['Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Wages<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace3 = {
    mode: 'markers', 
    name: 'Description=Overtime - Normal (Buyer)', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:0c48b8', 
    x: ['2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:0345c4', 
    y: [10726404.9615363, 0, 7150936.64102418, 0, 229325.49977827, 152883.666518847, 220831.962749446, 229325.49977827, 212338.425720621, 10329130.7037016, 0, 220831.962749446, 212338.425720621, 212338.425720621, 212338.425720621, 203844.888691796, 10726404.9615363, 0, 144390.129490022, 220831.962749446, 9931856.44586692, 0, 10329130.7037016, 0, 9931856.44586692, 0, 9931856.44586692, 0, 9931856.44586692, 0, 9534582.18803224, 0, 6753662.38318951, 0, 10329130.7037016, 0, 4801349.21574959, 0, 0, 3200899.47716639, 0, 0, 4623521.46701812, 0, 0, 4801349.21574959, 0, 0, 4445693.71828665, 0, 0, 4623521.46701812, 0, 0, 4445693.71828665, 0, 0, 4445693.71828665, 0, 0, 4445693.71828665, 0, 0, 4267865.96955519, 0, 0, 3023071.72843492, 0, 0, 4623521.46701812, 0, 0, 3362997.93933349, 2287830.4495513, 3243534.88491325, 3362997.93933349, 1031216.02490194, 1031216.02490194, 1048402.95865031, 962468.289908477, 2137375.45490759, 3004608.77607276, 2338545.76897808, 3243534.88491325, 19686663.6666706, 13839328.6318585, 19352611.2161978, 20016620.2098093, 16845452.4342116, 16750842.6688536, 16822670.3109249, 16378.0079825834, 19640793.2851611, 20616507.7338572, 16042895.3602932, 22091066.4794303], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#00cc96', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Overtime - Normal (Buyer)', 
    hovertextsrc: 'sumon23:120:e39389', 
    hovertext: ['Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Overtime - Normal (Buyer)<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace4 = {
    mode: 'markers', 
    name: 'Description=Incentive', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:a874d5', 
    x: ['2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:67faa3', 
    y: [2140320, 546000, 0, 2140320, 546000, 0, 2140320, 546000, 0, 2140320, 546000, 0, 2140320, 546000, 0, 2140320, 546000, 0, 2140320, 546000, 0, 2140320, 546000, 0, 2140320, 546000, 0, 2140320, 546000, 0, 2140320, 546000, 0, 2140320, 546000, 0, 1187676, 126000, 0, 1187676, 126000, 0, 1187676, 126000, 0, 1187676, 126000, 0, 1187676, 126000, 0, 1187676, 126000, 0, 1187676, 126000, 0, 1187676, 126000, 0, 1187676, 126000, 0, 1187676, 126000, 0, 1187676, 126000, 0, 1187676, 126000, 0, 3039523.13135566, 139829.371493738, 2947705.13135566, 137170.186130577, 3151571.13135566, 129079.739031383, 3161773.13135566, 125605.344395292, 3141369.13135566, 134357.033703167, 3120965.13135566, 134725.378704549, 3080157.13135566, 136576.824101274, 3141369.13135566, 423696, 3151571.13135566, 129626.43768249, 3131167.13135566, 123306.875180938, 3080157.13135566, 122810.346078415, 3151571.13135566, 122810.449392715, 7954761.4005913, 283866.628506262, 6463094.26706087, 286525.813869423, 8318940.60797681, 294616.260968617, 7780223.42233043, 298090.655604708, 7131068.81797101, 289338.966296833, 7312133.85440704, 288970.621295451, 6269752.85049241, 287119.175898726, 0, 7647843.37942797, 294069.56231751, 8587163.79945083, 300389.124819062, 8556167.67741496, 300885.653921585, 10880117.3201905, 300885.550607285], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#ab63fa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Incentive', 
    hovertextsrc: 'sumon23:120:b410c8', 
    hovertext: ['Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Incentive<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace5 = {
    mode: 'markers', 
    name: 'Description=Attendance Bonus', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:71c50c', 
    x: ['2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00'], 
    ysrc: 'sumon23:120:e82042', 
    y: [1146600, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FFA15A', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Attendance Bonus', 
    hovertextsrc: 'sumon23:120:e4c807', 
    hovertext: ['Direct Wages', 'Salaries', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Attendance Bonus<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace6 = {
    mode: 'markers', 
    name: 'Description=Daily Allowance ', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:827b0d', 
    x: ['2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:bbb1ff', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#19d3f3', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Daily Allowance ', 
    hovertextsrc: 'sumon23:120:446809', 
    hovertext: ['Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Daily Allowance <br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace7 = {
    mode: 'markers', 
    name: 'Description=Night Allowance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:c1cdf2', 
    x: ['2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:eda70d', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 134400, 0, 0, 134400, 0, 0, 134400, 0, 0, 134400, 0, 0, 134400, 0, 0, 134400, 0, 0, 134400, 0, 0, 134400, 0, 0, 134400, 0, 0, 134400, 0, 0, 134400, 0, 0, 134400, 0, 0, 218284.285714286, 13020, 154911.428571429, 9240, 211242.857142857, 12600, 218284.285714286, 13020, 211242.857142857, 12600, 211242.857142857, 12600, 214763.571428571, 12810, 197160, 11760, 211242.857142857, 12600, 197160, 11760, 147870, 8820, 211242.857142857, 12600], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF6692', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Night Allowance', 
    hovertextsrc: 'sumon23:120:c287fb', 
    hovertext: ['Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Night Allowance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace8 = {
    mode: 'markers', 
    name: 'Description=Maternity Allowance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:935e79', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:17fd2e', 
    y: [1092000, 1092000, 1092000, 1092000, 1092000, 1092000, 1092000, 1092000, 1092000, 1092000, 1092000, 1092000, 226800, 226800, 226800, 226800, 226800, 226800, 226800, 226800, 226800, 226800, 226800, 226800, 277649.283333333, 277649.283333333, 277649.283333333, 277649.283333333, 277649.283333333, 277649.283333333, 277649.283333333, 277649.283333333, 277649.283333333, 277649.283333333, 277649.283333333, 277649.283333333, 832000, 832000, 832000, 832000, 832000, 832000, 832000, 832000, 832000, 832000, 832000, 832000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#B6E880', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Maternity Allowance', 
    hovertextsrc: 'sumon23:120:c89984', 
    hovertext: ['Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Maternity Allowance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace9 = {
    mode: 'markers', 
    name: 'Description=Food Allowance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:8d2abb', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:ec5a18', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1228576, 830575.428571429, 1212274.4, 1258667.37142857, 1028615.71428571, 949960.742857143, 793407.942857143, 1025587.14285714, 1212274.4, 1119488.45714286, 884495.028571429, 1212274.4, 2428752.78571429, 1703589.81292517, 2555960.51020408, 2645760.52721088, 2521450.51020408, 2452430.51020408, 2342035.51870748, 0, 2555960.51020408, 2482320.47619048, 2106776.07142857, 2670750.51020408], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF97FF', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Food Allowance', 
    hovertextsrc: 'sumon23:120:273443', 
    hovertext: ['Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Food Allowance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace10 = {
    mode: 'markers', 
    name: 'Description=Friday /Holiday allowance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:06d836', 
    x: ['2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:0a4083', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FECB52', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Friday /Holiday allowance', 
    hovertextsrc: 'sumon23:120:2adddb', 
    hovertext: ['Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Friday /Holiday allowance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace11 = {
    mode: 'markers', 
    name: 'Description=Additional Allowance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:972d42', 
    x: ['2019-07-01T00:00:00', '2019-07-01T00:00:00'], 
    ysrc: 'sumon23:120:c15240', 
    y: [235200, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#636efa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Additional Allowance', 
    hovertextsrc: 'sumon23:120:33c973', 
    hovertext: ['Direct Wages', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Additional Allowance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace12 = {
    mode: 'markers', 
    name: 'Description=Thursday Allowance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:be5db7', 
    x: ['2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:cfcf89', 
    y: [522494.565981553, 0, 0, 522494.565981553, 0, 0, 522494.565981553, 0, 0, 522494.565981553, 0, 0, 522494.565981553, 0, 0, 522494.565981553, 0, 0, 522494.565981553, 0, 0, 522494.565981553, 0, 0, 522494.565981553, 0, 0, 522494.565981553, 0, 0, 522494.565981553, 0, 0, 522494.565981553, 0, 0, 100800, 0, 0, 100800, 0, 0, 100800, 0, 0, 100800, 0, 0, 100800, 0, 0, 100800, 0, 0, 100800, 0, 0, 100800, 0, 0, 100800, 0, 0, 100800, 0, 0, 100800, 0, 0, 100800, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#EF553B', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Thursday Allowance', 
    hovertextsrc: 'sumon23:120:d48885', 
    hovertext: ['Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Thursday Allowance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace13 = {
    mode: 'markers', 
    name: 'Description=Shift Allowance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:34102f', 
    x: ['2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:3e1503', 
    y: [378000, 0, 0, 378000, 0, 0, 378000, 0, 0, 378000, 0, 0, 378000, 0, 0, 378000, 0, 0, 378000, 0, 0, 378000, 0, 0, 378000, 0, 0, 378000, 0, 0, 378000, 0, 0, 378000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#00cc96', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Shift Allowance', 
    hovertextsrc: 'sumon23:120:70a7ba', 
    hovertext: ['Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Shift Allowance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace14 = {
    mode: 'markers', 
    name: 'Description=Provision for Earned Leave', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:ba464a', 
    x: ['2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:de599a', 
    y: [277126.383329814, 0, 0, 277126.383329814, 0, 0, 277126.383329814, 0, 0, 277126.383329814, 0, 0, 277126.383329814, 0, 0, 277126.383329814, 0, 0, 277126.383329814, 0, 0, 277126.383329814, 0, 0, 277126.383329814, 0, 0, 277126.383329814, 0, 0, 277126.383329814, 0, 0, 277126.383329814, 0, 0, 141768.233016474, 0, 0, 141768.233016474, 0, 0, 141768.233016474, 0, 0, 141768.233016474, 0, 0, 141768.233016474, 0, 0, 141768.233016474, 0, 0, 141768.233016474, 0, 0, 141768.233016474, 0, 0, 141768.233016474, 0, 0, 141768.233016474, 0, 0, 141768.233016474, 0, 0, 141768.233016474, 0, 0, 468470.72574786, 468470.72574786, 468470.72574786, 468470.72574786, 468470.72574786, 468470.72574786, 468470.72574786, 468470.72574786, 468470.72574786, 468470.72574786, 468470.72574786, 468470.72574786, 1256358.69124869, 1256358.69124869, 1256358.69124869, 1256358.69124869, 1256358.69124869, 1256358.69124869, 1256358.69124869, 1256358.69124869, 1256358.69124869, 1256358.69124869, 1256358.69124869, 1256358.69124869], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#ab63fa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Provision for Earned Leave', 
    hovertextsrc: 'sumon23:120:f66265', 
    hovertext: ['Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Provision for Earned Leave<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace15 = {
    mode: 'markers', 
    name: 'Description=Provision for EID Bonus', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:414091', 
    x: ['2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:9ae6e8', 
    y: [2205699.78568628, 1466259.24919416, 2205699.78568628, 1466259.24919416, 2205699.78568628, 1466259.24919416, 2205699.78568628, 1359789.24919416, 2205699.78568628, 1359789.24919416, 2205699.78568628, 1359789.24919416, 2205699.78568628, 1276524.24919416, 2205699.78568628, 1276524.24919416, 2205699.78568628, 1334117.36150856, 2205699.78568628, 1334117.36150856, 2205699.78568628, 1334117.36150856, 2205699.78568628, 1334117.36150856, 518477.82, 518477.82, 518477.82, 518477.82, 518477.82, 518477.82, 518477.82, 518477.82, 518477.82, 518477.82, 518477.82, 518477.82, 1215156.28299835, 586143.180781995, 0, 1215156.28299835, 586143.180781995, 0, 1215156.28299835, 586143.180781995, 0, 1215156.28299835, 586143.180781995, 0, 1215156.28299835, 586143.180781995, 0, 1215156.28299835, 586143.180781995, 0, 1215156.28299835, 586143.180781995, 0, 1215156.28299835, 586143.180781995, 0, 1215156.28299835, 603920.550781995, 0, 1215156.28299835, 603920.550781995, 0, 1215156.28299835, 603920.550781995, 0, 1215156.28299835, 603920.550781995, 0, 1904844.15485769, 365568, 335158.029611278, 1904844.15485769, 365568, 328784.209024321, 1904844.15485769, 365568, 309392.157987594, 1904844.15485769, 365568, 301064.356411395, 1904844.15485769, 365568, 322041.343669961, 1904844.15485769, 365568, 322924.232461934, 1904844.15485769, 365568, 327361.975294292, 1904844.15485769, 365568, 1015560, 1904844.15485769, 365568, 310702.543929679, 1904844.15485769, 365568, 295555.13896462, 1904844.15485769, 365568, 294365.004775582, 1904844.15485769, 365568, 294365.252410373, 3169835.54657814, 609252, 680401.970388722, 3169835.54657814, 609252, 686775.790975679, 3169835.54657814, 609252, 706167.842012406, 3169835.54657814, 609252, 714495.643588605, 3169835.54657814, 609252, 693518.656330039, 3169835.54657814, 609252, 692635.767538066, 3169835.54657814, 609252, 688198.024705708, 3169835.54657814, 609252, 0, 3169835.54657814, 609252, 704857.456070322, 3169835.54657814, 609252, 720004.86103538, 3169835.54657814, 609252, 721194.995224418, 3169835.54657814, 609252, 721194.747589627], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FFA15A', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Provision for EID Bonus', 
    hovertextsrc: 'sumon23:120:045c99', 
    hovertext: ['Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Provision for EID Bonus<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace16 = {
    mode: 'markers', 
    name: 'Description=Capitalization', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:0ecaf3', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:fe52e8', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#19d3f3', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Capitalization', 
    hovertextsrc: 'sumon23:120:66ac02', 
    hovertext: ['Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Capitalization<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace17 = {
    mode: 'markers', 
    name: 'Description=Salary', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:7a72e0', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:288904', 
    y: [22557834.602987, 22557834.602987, 22557834.602987, 20919834.602987, 20919834.602987, 20919834.602987, 19638834.602987, 19638834.602987, 20524882.484747, 20524882.484747, 20524882.484747, 20524882.484747, 9017587.39664606, 0, 9017587.39664606, 0, 9017587.39664606, 0, 9017587.39664606, 0, 9017587.39664606, 0, 9017587.39664606, 0, 9017587.39664606, 0, 9017587.39664606, 0, 9291085.39664606, 0, 9291085.39664606, 0, 9291085.39664606, 0, 9291085.39664606, 0, 17395897.2010242, 17395897.2010242, 17395897.2010242, 17395897.2010242, 17395897.2010242, 17395897.2010242, 17395897.2010242, 17395897.2010242, 17395897.2010242, 18313759.5010242, 18313759.5010242, 18313759.5010242, 5809860, 4489996.83222118, 5809860, 4404608.95033818, 5809860, 4144820.31324015, 5809860, 4033255.62019112, 5809860, 4314277.10265343, 5809860, 4326104.86009597, 5809860, 4385555.77428892, 5809860, 13605107.97912, 6216550.2, 4162375.10294867, 6216550.2, 3959450.52916369, 6216550.2, 3943506.71082552, 6216550.2, 3943510.02830364, 18272100, 9115111.14689882, 18572064, 9200499.02878182, 18572064, 9460287.66587985, 18572064, 9571852.35892888, 18572064, 9290830.87646657, 18572064, 9279003.11902404, 18572064, 9219552.20483108, 18572064, 0, 19872108.48, 9442732.87617133, 19872108.48, 9645657.44995631, 19872108.48, 9661601.26829448, 19872108.48, 9661597.95081636], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF6692', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Salary', 
    hovertextsrc: 'sumon23:120:915681', 
    hovertext: ['Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Salary<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace18 = {
    mode: 'markers', 
    name: 'Description=Salary ( Station common)', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:5c8a9d', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:791116', 
    y: [1652245.58475172, 1641506.49941012, 1670355.6205427, 1670355.6205427, 1670355.6205427, 1694925.28107718, 1694925.28107718, 1694925.28107718, 1694925.28107718, 1694925.28107718, 1694925.28107718, 1666076.15994459, 714612.613960769, 709967.852964984, 722445.384182673, 722445.384182673, 722445.384182673, 733072.006217986, 733072.006217986, 733072.006217986, 733072.006217986, 733072.006217986, 733072.006217986, 720594.475000298], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#B6E880', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Salary ( Station common)', 
    hovertextsrc: 'sumon23:120:2558b8', 
    hovertext: ['Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Salary ( Station common)<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace19 = {
    mode: 'markers', 
    name: 'Description=Provision for Gratuity', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:0af547', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:6d251d', 
    y: [733129.624597079, 733129.624597079, 733129.624597079, 679894.624597079, 679894.624597079, 679894.624597079, 638262.124597079, 638262.124597079, 667058.680754279, 667058.680754279, 667058.680754279, 667058.680754279, 161074.82, 161074.82, 161074.82, 161074.82, 161074.82, 161074.82, 161074.82, 161074.82, 161074.82, 161074.82, 161074.82, 161074.82, 293071.590390997, 0, 293071.590390997, 0, 293071.590390997, 0, 293071.590390997, 0, 293071.590390997, 0, 293071.590390997, 0, 293071.590390997, 0, 293071.590390997, 0, 301960.275390997, 0, 301960.275390997, 0, 301960.275390997, 0, 301960.275390997, 0, 1042020, 448124.859277611, 1042020, 439602.707930368, 1042020, 413674.461031386, 1042020, 402539.728816393, 1042020, 430587.123277496, 1042020, 431767.59452003, 1042020, 437701.102616396, 1042020, 1357860, 1042020, 415426.519654529, 1042020, 395173.599781893, 1042020, 393582.324416648, 1042020, 393582.655518088, 882168, 909735.140722389, 882168, 918257.292069633, 882168, 944185.538968614, 882168, 955320.271183607, 882168, 927272.876722504, 882168, 926092.40547997, 882168, 920158.897383604, 882168, 0, 882168, 942433.480345471, 882168, 962686.400218107, 882168, 964277.675583352, 882168, 964277.344481912], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF97FF', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Provision for Gratuity', 
    hovertextsrc: 'sumon23:120:66a15d', 
    hovertext: ['Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Provision for Gratuity<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace20 = {
    mode: 'markers', 
    name: 'Description=Electricity Meter Reading', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:7e42bf', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:ac713d', 
    y: [4409374.25364134, 2789909.4797758, 4321492.11643394, 4489240.88784013, 4015814.76627071, 3284560.35719498, 3484329.25744528, 3121222.73049681, 4014533.22508098, 4142919.46999126, 4416627.10286536, 3000121.08413271, 2502587.6123425, 1743690.98807963, 2544870.49781418, 2662995.27166689, 2068260.5719009, 1638712.7090145, 1845099.22882579, 1905665.95220836, 2607349.10491958, 2643208.5138882, 2001265.19895564, 2472748.13154685, 150281.25, 150281.25, 150281.25, 150281.25, 150281.25, 82320, 82320, 82320, 82320, 150281.25, 150281.25, 150281.25, 17929276.1694776, 13383805.8072267, 18430717.2429529, 18984487.3006549, 18571072.7950706, 18670257.9530689, 19513101.4274891, 507698.244336978, 17711508.783694, 16040398.7036612, 11932003.6936207, 17443872.5735388], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FECB52', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Electricity Meter Reading', 
    hovertextsrc: 'sumon23:120:84d619', 
    hovertext: ['Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Electricity Meter Reading<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace21 = {
    mode: 'markers', 
    name: 'Description=Gas Charges', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:35331c', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:8e37e9', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2299481.13168, 1632756.04416, 2225400.5664, 2299481.13168, 2272686.0336, 2225400.5664, 2262440.84904, 2077239.43584, 2225400.5664, 2077239.43584, 1558675.47888, 2225400.5664], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#636efa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Gas Charges', 
    hovertextsrc: 'sumon23:120:00cf58', 
    hovertext: ['Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Gas Charges<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace22 = {
    mode: 'markers', 
    name: 'Description=Generator fuel', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:943b23', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:5c3c5e', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2808000, 1872000, 2704000, 2808000, 2080000, 1913600, 1580800, 2080000, 2433600, 2496000, 1976000, 2704000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#EF553B', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Generator fuel', 
    hovertextsrc: 'sumon23:120:56053a', 
    hovertext: ['Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Generator fuel<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace23 = {
    mode: 'markers', 
    name: 'Description=Generator Rent', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:669ec8', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:255082', 
    y: [1490000, 1490000, 1490000, 1490000, 1490000, 1490000, 0, 0, 0, 0, 0, 0, 350000, 350000, 350000, 350000, 350000, 350000, 350000, 350000, 350000, 350000, 350000, 350000, 641000, 641000, 641000, 641000, 641000, 641000, 641000, 641000, 641000, 641000, 641000, 641000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#00cc96', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Generator Rent', 
    hovertextsrc: 'sumon23:120:bf5714', 
    hovertext: ['Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Generator Rent<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace24 = {
    mode: 'markers', 
    name: 'Description=Station OH', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:203b4d', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:38b1c9', 
    y: [713128.081610064, 713128.081610064, 713128.081610064, 713128.081610064, 713128.081610064, 713128.081610064, 713128.081610064, 713128.081610064, 713128.081610064, 713128.081610064, 713128.081610064, 713128.081610064, 308434.973100427, 308434.973100427, 308434.973100427, 308434.973100427, 308434.973100427, 308434.973100427, 308434.973100427, 308434.973100427, 308434.973100427, 308434.973100427, 308434.973100427, 308434.973100427, 1600084.51832467, 1587789.28409258, 1620818.79176776, 1620818.79176776, 1620818.79176776, 1648948.72291784, 1648948.72291784, 1648948.72291784, 1648948.72291784, 1648948.72291784, 1648948.72291784, 1615919.21524266], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#ab63fa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Station OH', 
    hovertextsrc: 'sumon23:120:d6383a', 
    hovertext: ['Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH', 'Station OH'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Station OH<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace25 = {
    mode: 'markers', 
    name: 'Description=Consumables', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:35e9c0', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:ef7f60', 
    y: [770896.996705107, 745896.996705107, 770896.996705107, 770896.996705107, 770896.996705107, 735896.996705107, 745896.996705107, 745896.996705107, 745896.996705107, 745896.996705107, 745896.996705107, 745896.996705107, 135506.668863262, 135506.668863262, 135506.668863262, 135506.668863262, 135506.668863262, 135506.668863262, 135506.668863262, 135506.668863262, 135506.668863262, 135506.668863262, 135506.668863262, 135506.668863262, 561248.382423978, 561248.382423978, 561248.382423978, 561248.382423978, 561248.382423978, 561248.382423978, 561248.382423978, 561248.382423978, 561248.382423978, 561248.382423978, 561248.382423978, 562481.382423978, 1705156.68598511, 1501219.3887161, 1804211.03215689, 1832379.96116656, 1804211.03215689, 1804211.03215689, 1818295.49666172, 0, 2032761.03215689, 1976423.17413754, 1779240.67106982, 2032761.03215689], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FFA15A', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Consumables', 
    hovertextsrc: 'sumon23:120:3a9295', 
    hovertext: ['Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Consumables<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace26 = {
    mode: 'markers', 
    name: 'Description=Needles', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:8aa5d1', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:2dd0f0', 
    y: [75000, 70000, 75000, 75000, 75000, 75000, 75000, 75000, 75000, 75000, 75000, 75000, 41037.7358490566, 41037.7358490566, 41037.7358490566, 41037.7358490566, 41037.7358490566, 41037.7358490566, 41037.7358490566, 41037.7358490566, 41037.7358490566, 41037.7358490566, 41037.7358490566, 41037.7358490566, 332511.3, 239380.2, 322163.4, 332511.3, 320707.5, 317795.7, 316418.1, 302923.5, 322163.4, 301467.6, 231944.1, 322163.4, 2091552.18270353, 1516532.3495633, 2368195.82941798, 2457196.12480318, 2084301.22084974, 2105916.4334555, 1919459.40985666, 0, 2205499.34796037, 2276821.33897514, 1742612.23423136, 2436584.21461623], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#19d3f3', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Needles', 
    hovertextsrc: 'sumon23:120:ad3260', 
    hovertext: ['Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Needles<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace27 = {
    mode: 'markers', 
    name: 'Description=Spare Parts', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:13401e', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:92490c', 
    y: [725000, 630000, 725000, 725000, 725000, 725000, 725000, 725000, 725000, 725000, 725000, 725000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 205776.791666667, 205776.791666667, 205776.791666667, 205776.791666667, 205776.791666667, 205776.791666667, 205776.791666667, 205776.791666667, 205776.791666667, 205776.791666667, 205776.791666667, 205776.791666667, 1393028.85688889, 1393028.85688889, 1393028.85688889, 1393028.85688889, 1393028.85688889, 1393028.85688889, 1393028.85688889, 0, 1393028.85688889, 1393028.85688889, 1393028.85688889, 1393028.85688889], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF6692', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Spare Parts', 
    hovertextsrc: 'sumon23:120:98f6e8', 
    hovertext: ['Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Spare Parts<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace28 = {
    mode: 'markers', 
    name: 'Description=Chemical ETP', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:7c5280', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:93ea9f', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 93741.75, 93741.75, 93741.75, 93741.75, 93741.75, 93741.75, 93741.75, 93741.75, 93741.75, 93741.75, 93741.75, 93741.75], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#B6E880', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Chemical ETP', 
    hovertextsrc: 'sumon23:120:b42a36', 
    hovertext: ['Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Chemical ETP<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace29 = {
    mode: 'markers', 
    name: 'Description=CSR Projects', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:98a1ad', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:2e4c10', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF97FF', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=CSR Projects', 
    hovertextsrc: 'sumon23:120:38610f', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=CSR Projects<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace30 = {
    mode: 'markers', 
    name: 'Description=T&D', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:4736c5', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:f12ae6', 
    y: [217475, 217475, 217475, 217475, 217475, 217475, 217475, 217475, 217475, 217475, 217475, 217475, 68800, 68800, 68800, 68800, 68800, 68800, 68800, 68800, 68800, 68800, 68800, 68800, 15500, 15500, 15500, 15500, 15500, 15500, 15500, 15500, 15500, 15500, 15500, 15500], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FECB52', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=T&D', 
    hovertextsrc: 'sumon23:120:24f748', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=T&D<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace31 = {
    mode: 'markers', 
    name: 'Description=Annual Trip', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:95509b', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:2160dc', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#636efa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Annual Trip', 
    hovertextsrc: 'sumon23:120:485f3a', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Annual Trip<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace32 = {
    mode: 'markers', 
    name: 'Description=Fire & All Risk Insurance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:6410b7', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:a1250d', 
    y: [291830, 291830, 291830, 291830, 291830, 291830, 291830, 291830, 291830, 291830, 291830, 291830, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333, 291785.833333333], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#EF553B', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Fire & All Risk Insurance', 
    hovertextsrc: 'sumon23:120:d5b7ad', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Fire & All Risk Insurance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace33 = {
    mode: 'markers', 
    name: 'Description=Group Insurance - Workers & staff', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:162f20', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:e91b09', 
    y: [207137.066675, 207137.066675, 207137.066675, 207137.066675, 207137.066675, 207137.066675, 207137.066675, 207137.066675, 207137.066675, 207137.066675, 207137.066675, 207137.066675, 41882.1375, 41882.1375, 41882.1375, 41882.1375, 41882.1375, 41882.1375, 41882.1375, 41882.1375, 41882.1375, 41882.1375, 41882.1375, 41882.1375], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#00cc96', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Group Insurance - Workers & staff', 
    hovertextsrc: 'sumon23:120:7b1621', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Group Insurance - Workers & staff<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace34 = {
    mode: 'markers', 
    name: 'Description=Security Sercvices Charges', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:d0cee4', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:8657a1', 
    y: [437421.366666667, 437421.366666667, 437421.366666667, 437421.366666667, 437421.366666667, 437421.366666667, 437421.366666667, 437421.366666667, 437421.366666667, 437421.366666667, 437421.366666667, 437421.366666667, 173500.8, 173500.8, 173500.8, 173500.8, 173500.8, 173500.8, 173500.8, 173500.8, 173500.8, 173500.8, 173500.8, 173500.8, 880610, 880610, 880610, 880610, 880610, 880610, 880610, 880610, 880610, 880610, 880610, 880610, 612443.333333333, 612443.333333333, 612443.333333333, 612443.333333333, 612443.333333333, 612443.333333333, 612443.333333333, 612443.333333333, 612443.333333333, 612443.333333333, 612443.333333333, 612443.333333333], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#ab63fa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Security Sercvices Charges', 
    hovertextsrc: 'sumon23:120:e9d047', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Security Sercvices Charges<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace35 = {
    mode: 'markers', 
    name: 'Description=Telephone, Fax & Email , Internet', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:eea175', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:d6eab6', 
    y: [145570.833333333, 145570.833333333, 145570.833333333, 145570.833333333, 145570.833333333, 145570.833333333, 145570.833333333, 145570.833333333, 145570.833333333, 145570.833333333, 145570.833333333, 145570.833333333, 67182.5, 67182.5, 67182.5, 67182.5, 67182.5, 67182.5, 67182.5, 67182.5, 67182.5, 67182.5, 67182.5, 67182.5, 160338.333333333, 160338.333333333, 160338.333333333, 160338.333333333, 160338.333333333, 160338.333333333, 160338.333333333, 160338.333333333, 160338.333333333, 160338.333333333, 160338.333333333, 160338.333333333, 217975.833333333, 217975.833333333, 217975.833333333, 217975.833333333, 217975.833333333, 217975.833333333, 217975.833333333, 217975.833333333, 217975.833333333, 217975.833333333, 217975.833333333, 217975.833333333], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FFA15A', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Telephone, Fax & Email , Internet', 
    hovertextsrc: 'sumon23:120:464894', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Telephone, Fax & Email , Internet<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace36 = {
    mode: 'markers', 
    name: 'Description=Postage & Courier Charges Local', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:61feb4', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:f6f49a', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 26992.2879177378, 26992.2879177378, 26992.2879177378, 26992.2879177378, 26992.2879177378, 26992.2879177378, 26992.2879177378, 5398.45758354756, 26992.2879177378, 26992.2879177378, 26992.2879177378, 26992.2879177378], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#19d3f3', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Postage & Courier Charges Local', 
    hovertextsrc: 'sumon23:120:266b71', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Postage & Courier Charges Local<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace37 = {
    mode: 'markers', 
    name: 'Description=WFX,FR & Kormee Charges', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:77a1c4', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:abb4ba', 
    y: [533442.5, 533442.5, 533442.5, 533442.5, 533442.5, 533442.5, 533442.5, 533442.5, 533442.5, 533442.5, 533442.5, 533442.5, 111730, 111730, 111730, 111730, 111730, 111730, 111730, 111730, 111730, 111730, 111730, 111730, 96416.6666666667, 96416.6666666667, 96416.6666666667, 96416.6666666667, 96416.6666666667, 96416.6666666667, 96416.6666666667, 96416.6666666667, 96416.6666666667, 96416.6666666667, 96416.6666666667, 96416.6666666667, 170718.75, 170718.75, 170718.75, 170718.75, 170718.75, 170718.75, 170718.75, 170718.75, 170718.75, 170718.75, 170718.75, 170718.75], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF6692', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=WFX,FR & Kormee Charges', 
    hovertextsrc: 'sumon23:120:b5797d', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=WFX,FR & Kormee Charges<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace38 = {
    mode: 'markers', 
    name: 'Description=Vehicle  Hire -  staff ', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:b7a0d2', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:ad6533', 
    y: [465463.917525773, 465463.917525773, 465463.917525773, 465463.917525773, 465463.917525773, 465463.917525773, 465463.917525773, 465463.917525773, 465463.917525773, 465463.917525773, 465463.917525773, 465463.917525773, 199484.536082474, 199484.536082474, 199484.536082474, 199484.536082474, 199484.536082474, 199484.536082474, 199484.536082474, 199484.536082474, 199484.536082474, 199484.536082474, 199484.536082474, 199484.536082474, 206000, 206000, 206000, 206000, 206000, 206000, 206000, 206000, 206000, 206000, 206000, 206000, 514000, 514000, 514000, 514000, 514000, 514000, 514000, 514000, 514000, 718000, 718000, 718000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#B6E880', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Vehicle  Hire -  staff ', 
    hovertextsrc: 'sumon23:120:6e8fe5', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Vehicle  Hire -  staff <br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace39 = {
    mode: 'markers', 
    name: 'Description=Adhoc Hiring', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:4ec3e2', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:4188a2', 
    y: [252175, 151305, 252175, 252175, 252175, 252175, 252175, 252175, 252175, 201740, 151305, 252175, 108075, 64845, 108075, 108075, 108075, 108075, 108075, 108075, 108075, 86460, 64845, 108075, 95280, 95280, 95280, 95280, 95280, 95280, 95280, 95280, 95280, 95280, 95280, 95280, 112492.222222222, 112492.222222222, 112492.222222222, 112492.222222222, 112492.222222222, 112492.222222222, 112492.222222222, 44996.8888888889, 112492.222222222, 112492.222222222, 112492.222222222, 112492.222222222], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF97FF', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Adhoc Hiring', 
    hovertextsrc: 'sumon23:120:5b8355', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Adhoc Hiring<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace40 = {
    mode: 'markers', 
    name: 'Description=Vehicle Fuel Expenses Factory', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:784fb6', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:6f858f', 
    y: [382982.25, 263802, 369740, 382982.25, 356497.75, 369740, 356497.75, 356497.75, 356497.75, 343255.5, 250559.75, 369740, 164135.25, 113058, 158460, 164135.25, 152784.75, 158460, 152784.75, 152784.75, 152784.75, 147109.5, 107382.75, 158460, 266927.978878305, 266927.978878305, 266927.978878305, 266927.978878305, 266927.978878305, 266927.978878305, 266927.978878305, 266927.978878305, 266927.978878305, 266927.978878305, 266927.978878305, 266927.978878305, 601413.2, 601413.2, 601413.2, 601413.2, 601413.2, 601413.2, 601413.2, 601413.2, 751413.2, 751413.2, 751413.2, 751413.2], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FECB52', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Vehicle Fuel Expenses Factory', 
    hovertextsrc: 'sumon23:120:5d5109', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Vehicle Fuel Expenses Factory<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace41 = {
    mode: 'markers', 
    name: 'Description=Lisence & Tax Token', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:5dcbcc', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:362f82', 
    y: [0, 0, 0, 0, 0, 2100, 0, 0, 0, 0, 79940, 37870, 0, 0, 0, 0, 0, 900, 0, 0, 0, 0, 34260, 16230, 4166.66666666667, 4166.66666666667, 4166.66666666667, 4166.66666666667, 4166.66666666667, 4166.66666666667, 4166.66666666667, 4166.66666666667, 4166.66666666667, 4166.66666666667, 4166.66666666667, 4166.66666666667, 43409, 43409, 43409, 43409, 43409, 43409, 43409, 43409, 43409, 43409, 43409, 43409], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#636efa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Lisence & Tax Token', 
    hovertextsrc: 'sumon23:120:38fddd', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Lisence & Tax Token<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace42 = {
    mode: 'markers', 
    name: 'Description=Driver OT', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:7c0b77', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:678bcb', 
    y: [127281, 82908, 123648, 127281, 120015, 123648, 120015, 120015, 120015, 110544, 79275, 123648, 54549, 35532, 52992, 54549, 51435, 52992, 51435, 51435, 51435, 47376, 33975, 52992, 49143.375, 49143.375, 49143.375, 49143.375, 49143.375, 49143.375, 49143.375, 49143.375, 49143.375, 49143.375, 49143.375, 49143.375, 91355, 91355, 91355, 91355, 91355, 91355, 91355, 91355, 123355, 123355, 123355, 123355], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#EF553B', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Driver OT', 
    hovertextsrc: 'sumon23:120:895394', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Driver OT<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace43 = {
    mode: 'markers', 
    name: 'Description=Genarator Fuel Transpsort', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:bea12a', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:53e17c', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#00cc96', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Genarator Fuel Transpsort', 
    hovertextsrc: 'sumon23:120:abb0ef', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Genarator Fuel Transpsort<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace44 = {
    mode: 'markers', 
    name: 'Description=Foreign Travel-Operation', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:05c79f', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:4eb01f', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 76166.6666666667, 76166.6666666667, 76166.6666666667, 76166.6666666667, 76166.6666666667, 76166.6666666667, 76166.6666666667, 76166.6666666667, 76166.6666666667, 76166.6666666667, 76166.6666666667, 76166.6666666667], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#ab63fa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Foreign Travel-Operation', 
    hovertextsrc: 'sumon23:120:c38434', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Foreign Travel-Operation<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace45 = {
    mode: 'markers', 
    name: 'Description=Annual Trip Expact', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:2066f9', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:1f6711', 
    y: [96250, 96250, 96250, 96250, 96250, 96250, 96250, 96250, 96250, 96250, 96250, 96250, 13750, 13750, 13750, 13750, 13750, 13750, 13750, 13750, 13750, 13750, 13750, 13750, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000, 30000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FFA15A', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Annual Trip Expact', 
    hovertextsrc: 'sumon23:120:dee3eb', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Annual Trip Expact<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace46 = {
    mode: 'markers', 
    name: 'Description=Expact VISA', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:ff692c', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:e2bcd7', 
    y: [46708.3333333333, 46708.3333333333, 46708.3333333333, 46708.3333333333, 46708.3333333333, 46708.3333333333, 46708.3333333333, 46708.3333333333, 46708.3333333333, 46708.3333333333, 46708.3333333333, 46708.3333333333, 4916.66666666667, 4916.66666666667, 4916.66666666667, 4916.66666666667, 4916.66666666667, 4916.66666666667, 4916.66666666667, 4916.66666666667, 4916.66666666667, 4916.66666666667, 4916.66666666667, 4916.66666666667], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#19d3f3', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Expact VISA', 
    hovertextsrc: 'sumon23:120:d0cfde', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Expact VISA<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace47 = {
    mode: 'markers', 
    name: 'Description=Repair Maintenance - Factory Maintenance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:3bb11a', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:156c1a', 
    y: [478520, 436613.333333333, 473863.703703704, 478520, 469207.407407407, 473863.703703704, 469207.407407407, 469207.407407407, 469207.407407407, 464551.111111111, 431957.037037037, 473863.703703704, 69060, 46040, 66502.2222222222, 69060, 63944.4444444444, 66502.2222222222, 63944.4444444444, 63944.4444444444, 63944.4444444444, 61386.6666666667, 43482.2222222222, 66502.2222222222, 216000, 216000, 216000, 216000, 216000, 216000, 216000, 216000, 216000, 216000, 216000, 216000, 294000, 294000, 294000, 294000, 294000, 294000, 294000, 294000, 294000, 294000, 294000, 294000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF6692', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Repair Maintenance - Factory Maintenance', 
    hovertextsrc: 'sumon23:120:bd8b06', 
    hovertext: ['Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Repair Maintenance - Factory Maintenance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace48 = {
    mode: 'markers', 
    name: 'Description=Repair Maintenance - Equipment', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:cac786', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:5b89c1', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 379655, 379655, 379655, 379655, 379655, 379655, 379655, 379655, 379655, 379655, 379655, 379655, 259095, 259095, 259095, 259095, 259095, 259095, 259095, 259095, 259095, 259095, 259095, 259095], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#B6E880', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Repair Maintenance - Equipment', 
    hovertextsrc: 'sumon23:120:87d84b', 
    hovertext: ['Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Repair Maintenance - Equipment<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace49 = {
    mode: 'markers', 
    name: 'Description=Repair Maintenance - Electrical', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:9f6230', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:ddc203', 
    y: [218400, 218400, 218400, 218400, 218400, 218400, 218400, 218400, 218400, 218400, 218400, 218400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 302000, 302000, 302000, 302000, 302000, 302000, 302000, 302000, 302000, 302000, 302000, 302000, 230000, 230000, 230000, 230000, 230000, 230000, 230000, 230000, 230000, 230000, 230000, 230000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF97FF', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Repair Maintenance - Electrical', 
    hovertextsrc: 'sumon23:120:a1f1b3', 
    hovertext: ['Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Repair Maintenance - Electrical<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace50 = {
    mode: 'markers', 
    name: 'Description=Repair Maintenance - Computer', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:4916a0', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:6adba0', 
    y: [91863.3333333333, 91863.3333333333, 91863.3333333333, 91863.3333333333, 91863.3333333333, 91863.3333333333, 91863.3333333333, 91863.3333333333, 91863.3333333333, 91863.3333333333, 91863.3333333333, 91863.3333333333, 67318.3333333333, 67318.3333333333, 67318.3333333333, 67318.3333333333, 67318.3333333333, 67318.3333333333, 67318.3333333333, 67318.3333333333, 67318.3333333333, 67318.3333333333, 67318.3333333333, 67318.3333333333, 93608.2166666667, 93608.2166666667, 93608.2166666667, 93608.2166666667, 93608.2166666667, 93608.2166666667, 93608.2166666667, 93608.2166666667, 93608.2166666667, 93608.2166666667, 93608.2166666667, 93608.2166666667, 141260.166666667, 141260.166666667, 141260.166666667, 141260.166666667, 141260.166666667, 141260.166666667, 141260.166666667, 141260.166666667, 141260.166666667, 141260.166666667, 141260.166666667, 141260.166666667], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FECB52', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Repair Maintenance - Computer', 
    hovertextsrc: 'sumon23:120:f2a72d', 
    hovertext: ['Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Repair Maintenance - Computer<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace51 = {
    mode: 'markers', 
    name: 'Description=Repair Maintenance - Vehicle', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:e32050', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:6fa85d', 
    y: [58800, 58800, 58800, 58800, 58800, 58800, 58800, 58800, 58800, 58800, 58800, 58800, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12500, 12500, 12500, 12500, 12500, 12500, 12500, 12500, 12500, 12500, 12500, 12500, 23916.6666666667, 23916.6666666667, 23916.6666666667, 23916.6666666667, 23916.6666666667, 23916.6666666667, 23916.6666666667, 23916.6666666667, 23916.6666666667, 23916.6666666667, 23916.6666666667, 23916.6666666667], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#636efa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Repair Maintenance - Vehicle', 
    hovertextsrc: 'sumon23:120:a1b357', 
    hovertext: ['Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Repair Maintenance - Vehicle<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace52 = {
    mode: 'markers', 
    name: 'Description=Repair Maintenance - Plant & Machinery', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:41d9ab', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:1d0919', 
    y: [428809.666666667, 428809.666666667, 428809.666666667, 428809.666666667, 428809.666666667, 428809.666666667, 428809.666666667, 428809.666666667, 428809.666666667, 428809.666666667, 428809.666666667, 428809.666666667, 134054.827672956, 134054.827672956, 134054.827672956, 134054.827672956, 134054.827672956, 134054.827672956, 134054.827672956, 134054.827672956, 134054.827672956, 134054.827672956, 134054.827672956, 134054.827672956], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#EF553B', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Repair Maintenance - Plant & Machinery', 
    hovertextsrc: 'sumon23:120:d4eb22', 
    hovertext: ['Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Repair Maintenance - Plant & Machinery<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace53 = {
    mode: 'markers', 
    name: 'Description=Repair Maintenance - ETP', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:49f1e7', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:0a2306', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#00cc96', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Repair Maintenance - ETP', 
    hovertextsrc: 'sumon23:120:aa4f0d', 
    hovertext: ['Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance', 'Repair & Maintenance'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Repair Maintenance - ETP<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace54 = {
    mode: 'markers', 
    name: 'Description=AC Rent', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:7232f7', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:961022', 
    y: [261500, 261500, 261500, 261500, 261500, 261500, 261500, 261500, 261500, 261500, 261500, 261500, 62500, 62500, 62500, 62500, 62500, 62500, 62500, 62500, 62500, 62500, 62500, 62500, 295740, 295740, 295740, 295740, 295740, 295740, 295740, 295740, 295740, 295740, 295740, 295740], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#ab63fa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=AC Rent', 
    hovertextsrc: 'sumon23:120:151756', 
    hovertext: ['Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=AC Rent<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace55 = {
    mode: 'markers', 
    name: 'Description=Industrial Fans', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:fe9732', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:331093', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FFA15A', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Industrial Fans', 
    hovertextsrc: 'sumon23:120:4a49d0', 
    hovertext: ['Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Industrial Fans<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace56 = {
    mode: 'markers', 
    name: 'Description=Machine Rent', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:d1a9c3', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00'], 
    ysrc: 'sumon23:120:9a77dc', 
    y: [35227.896760022, 35227.896760022, 35227.896760022, 35227.896760022, 35227.896760022, 35227.896760022, 35227.896760022, 35227.896760022, 35227.896760022, 35227.896760022, 35227.896760022, 35227.896760022, 14772.103239978, 14772.103239978, 14772.103239978, 14772.103239978, 14772.103239978, 14772.103239978, 14772.103239978, 14772.103239978, 14772.103239978, 14772.103239978, 14772.103239978, 14772.103239978, 23250, 23250, 23250, 23250, 23250, 23250, 23250, 23250, 23250, 23250, 23250, 23250, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#19d3f3', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Machine Rent', 
    hovertextsrc: 'sumon23:120:beb957', 
    hovertext: ['Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Machine Rent<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace57 = {
    mode: 'markers', 
    name: 'Description=House Rent/Office Rent', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:c216c5', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:08212e', 
    y: [481950, 481950, 481950, 481950, 481950, 481950, 481950, 481950, 481950, 481950, 481950, 481950, 206550, 206550, 206550, 206550, 206550, 206550, 206550, 206550, 206550, 206550, 206550, 206550], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF6692', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=House Rent/Office Rent', 
    hovertextsrc: 'sumon23:120:9deaf0', 
    hovertext: ['Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=House Rent/Office Rent<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace58 = {
    mode: 'markers', 
    name: 'Description=Table & Chair', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:eee9ba', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:45b209', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 9300, 9300, 9300, 9300, 9300, 9300, 9300, 9300, 9300, 9300, 9300, 9300], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#B6E880', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Table & Chair', 
    hovertextsrc: 'sumon23:120:c5ebfb', 
    hovertext: ['Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Table & Chair<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace59 = {
    mode: 'markers', 
    name: 'Description=TV Rent', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:7b99c9', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:e2ddfa', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF97FF', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=TV Rent', 
    hovertextsrc: 'sumon23:120:9d530c', 
    hovertext: ['Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=TV Rent<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace60 = {
    mode: 'markers', 
    name: 'Description=Refrigerator', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:4dea81', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:fa793b', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FECB52', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Refrigerator', 
    hovertextsrc: 'sumon23:120:7d214c', 
    hovertext: ['Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates', 'Rent & Rates'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Refrigerator<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace61 = {
    mode: 'markers', 
    name: 'Description=Development sample courier', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:2ce2a8', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:16c626', 
    y: [1680000, 1680000, 1680000, 1680000, 1680000, 1680000, 1680000, 1680000, 1680000, 1680000, 1680000, 1680000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 652657.195296801, 640245.379078543, 602483.008726379, 586266.182216168, 627114.917619678, 628834.177407056, 637475.846513938, 1977612, 605034.738770589, 575538.017919277, 573220.455536106, 573220.937758265, 1324954.8047032, 1337366.62092146, 1375128.99127362, 1391345.81778383, 1350497.08238032, 1348777.82259294, 1340136.15348606, 0, 1372577.26122941, 1402073.98208072, 1404391.54446389, 1404391.06224174], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#636efa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Development sample courier', 
    hovertextsrc: 'sumon23:120:0c70e7', 
    hovertext: ['Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier', 'Development Sample Courier'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Development sample courier<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace62 = {
    mode: 'markers', 
    name: 'Description=Photo Shoot', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:db7f39', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:97692d', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#EF553B', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Photo Shoot', 
    hovertextsrc: 'sumon23:120:7e9e00', 
    hovertext: ['Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses '], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Photo Shoot<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace63 = {
    mode: 'markers', 
    name: 'Description=Buyer QC entertainment', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:32bcff', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:f66398', 
    y: [130000, 78000, 130000, 130000, 130000, 130000, 130000, 130000, 130000, 104000, 78000, 130000, 126000, 126000, 126000, 126000, 126000, 126000, 126000, 126000, 126000, 126000, 126000, 126000, 81786.0059090909, 81786.0059090909, 81786.0059090909, 81786.0059090909, 81786.0059090909, 81786.0059090909, 81786.0059090909, 81786.0059090909, 81786.0059090909, 81786.0059090909, 81786.0059090909, 81786.0059090909, 164838.492247193, 166382.654405936, 171080.695554536, 173098.241527794, 168016.223688229, 167802.329455666, 166727.213760382, 0, 170763.233153844, 174432.93945183, 174721.268900936, 174721.208907363], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#00cc96', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Buyer QC entertainment', 
    hovertextsrc: 'sumon23:120:7e8afe', 
    hovertext: ['Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses '], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Buyer QC entertainment<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace64 = {
    mode: 'markers', 
    name: 'Description=Buyer Visit Expenses', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:05e785', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:8ce675', 
    y: [90000, 54000, 90000, 90000, 90000, 90000, 90000, 90000, 90000, 72000, 54000, 90000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#ab63fa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Buyer Visit Expenses', 
    hovertextsrc: 'sumon23:120:ad4abe', 
    hovertext: ['Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses '], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Buyer Visit Expenses<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace65 = {
    mode: 'markers', 
    name: 'Description=Foreign Travel', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:92a85d', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:b6d933', 
    y: [475328.333333333, 475328.333333333, 475328.333333333, 475328.333333333, 475328.333333333, 475328.333333333, 475328.333333333, 475328.333333333, 475328.333333333, 475328.333333333, 475328.333333333, 475328.333333333, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 244069.516318536, 222440.69723746, 239427.958694021, 218210.462631195, 225306.236703807, 205340.171686721, 219241.746752434, 199813.101393303, 234517.654478579, 213735.297072603, 235160.593693461, 214321.260651328, 238392.256523965, 217266.541750322, 739553.333333333, 674016, 226260.488832113, 206209.860421153, 215229.8123253, 196156.694379828, 214363.130192666, 195366.815411023, 214363.310525818, 195366.97976351, 495483.817014797, 451575.30276254, 500125.374639313, 455805.537368805, 514247.096629526, 468675.828313279, 520311.5865809, 474202.898606697, 505035.678854754, 460280.702927397, 504392.739639872, 459694.739348672, 501161.076809369, 456749.458249678, 0, 0, 513292.844501221, 467806.139578847, 524323.521008034, 477859.305620173, 525190.203140667, 478649.184588977, 525190.022807515, 478649.02023649], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FFA15A', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Foreign Travel', 
    hovertextsrc: 'sumon23:120:b7bee9', 
    hovertext: ['Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel', 'Salaries', 'Foreign Travel'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Foreign Travel<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace66 = {
    mode: 'markers', 
    name: 'Description=Testing & Audit ', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:fa3daf', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:7d3cfa', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#19d3f3', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Testing & Audit ', 
    hovertextsrc: 'sumon23:120:7e86e4', 
    hovertext: ['Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses ', 'Selling Expenses '], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Testing & Audit <br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace67 = {
    mode: 'markers', 
    name: 'Description=Printing & Stationery', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:bf144a', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:663f2e', 
    y: [455455.656781988, 455455.656781988, 455455.656781988, 455455.656781988, 455455.656781988, 455455.656781988, 455455.656781988, 455455.656781988, 455455.656781988, 455455.656781988, 455455.656781988, 455455.656781988, 237299.676551345, 237299.676551345, 237299.676551345, 237299.676551345, 237299.676551345, 237299.676551345, 237299.676551345, 237299.676551345, 237299.676551345, 237299.676551345, 237299.676551345, 237299.676551345, 207361.792777778, 207361.792777778, 207361.792777778, 207361.792777778, 207361.792777778, 207361.792777778, 207361.792777778, 207361.792777778, 207361.792777778, 207361.792777778, 207361.792777778, 207361.792777778, 549728.072222222, 549728.072222222, 549728.072222222, 549728.072222222, 549728.072222222, 549728.072222222, 549728.072222222, 119545.614444444, 605609.686666667, 605609.686666667, 605609.686666667, 605609.686666667], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF6692', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Printing & Stationery', 
    hovertextsrc: 'sumon23:120:1b6d45', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Printing & Stationery<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace68 = {
    mode: 'markers', 
    name: 'Description=Entertainment & Daily Tea Coffee Exp', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:d81185', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:8798fc', 
    y: [369724.5, 321183, 364331, 369724.5, 358937.5, 405413.75, 402321.875, 407634.375, 414275, 415833.90625, 379016.7578125, 452663.603515625, 71865, 47910, 69203.3333333333, 71865, 66541.6666666667, 69203.3333333333, 66541.6666666667, 66541.6666666667, 66541.6666666667, 63880, 45248.3333333333, 69203.3333333333, 76889.6845454545, 76889.6845454545, 76889.6845454545, 76889.6845454545, 76889.6845454545, 76889.6845454545, 76889.6845454545, 76889.6845454545, 76889.6845454545, 76889.6845454545, 76889.6845454545, 76889.6845454545, 356948, 356948, 356948, 356948, 356948, 356948, 356948, 356948, 356948, 356948, 356948, 356948], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#B6E880', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Entertainment & Daily Tea Coffee Exp', 
    hovertextsrc: 'sumon23:120:6d3c57', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Entertainment & Daily Tea Coffee Exp<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace69 = {
    mode: 'markers', 
    name: 'Description=Tifin Expense', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:c9c9aa', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:9e71c7', 
    y: [546750, 364500, 526500, 546750, 506250, 526500, 506250, 506250, 506250, 486000, 8032500, 526500, 176850, 117900, 170300, 176850, 163750, 170300, 163750, 163750, 163750, 157200, 4777000, 170300, 1966492.34693878, 1312276.09329446, 1907623.03206997, 1980993.14868805, 1834252.9154519, 1687512.68221574, 1394032.21574344, 1907623.03206997, 2127264.13994169, 1684084.11078717, 2304536.1516035], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF97FF', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Tifin Expense', 
    hovertextsrc: 'sumon23:120:6859f7', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Tifin Expense<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace70 = {
    mode: 'markers', 
    name: 'Description=Welfare', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:f85304', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:5b022b', 
    y: [421156.666666667, 421156.666666667, 421156.666666667, 421156.666666667, 421156.666666667, 421156.666666667, 421156.666666667, 421156.666666667, 421156.666666667, 421156.666666667, 421156.666666667, 886156.666666667, 149586.666666667, 149586.666666667, 149586.666666667, 149586.666666667, 149586.666666667, 149586.666666667, 149586.666666667, 149586.666666667, 149586.666666667, 149586.666666667, 149586.666666667, 479586.666666667], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FECB52', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Welfare', 
    hovertextsrc: 'sumon23:120:3cdfcf', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Welfare<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace71 = {
    mode: 'markers', 
    name: 'Description=Medical Expense', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:4fa5aa', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:493372', 
    y: [65000, 65000, 65000, 65000, 65000, 65000, 65000, 65000, 65000, 65000, 65000, 65000, 40000, 40000, 40000, 40000, 40000, 40000, 40000, 40000, 40000, 40000, 40000, 40000, 32404.75, 32404.75, 32404.75, 32404.75, 32404.75, 32404.75, 32404.75, 32404.75, 32404.75, 32404.75, 32404.75, 32404.75, 190338, 190338, 190338, 190338, 190338, 190338, 190338, 190338, 190338, 190338, 190338, 190338], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#636efa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Medical Expense', 
    hovertextsrc: 'sumon23:120:d8b27a', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Medical Expense<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace72 = {
    mode: 'markers', 
    name: 'Description=Child Care Exp', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:b6fd2a', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:7abd0c', 
    y: [54000, 36000, 52000, 54000, 50000, 52000, 50000, 50000, 50000, 48000, 34000, 52000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#EF553B', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Child Care Exp', 
    hovertextsrc: 'sumon23:120:b9cd58', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Child Care Exp<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace73 = {
    mode: 'markers', 
    name: 'Description=Best Line Gift', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:a32d33', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:7d86b6', 
    y: [80000, 80000, 80000, 80000, 80000, 80000, 80000, 80000, 80000, 80000, 80000, 80000, 40000, 40000, 40000, 40000, 40000, 40000, 40000, 40000, 40000, 40000, 40000, 40000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#00cc96', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Best Line Gift', 
    hovertextsrc: 'sumon23:120:7f4722', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Best Line Gift<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace74 = {
    mode: 'markers', 
    name: 'Description=Compliance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:abb04b', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:9ddc96', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1144381.64583333, 1144381.64583333, 1144381.64583333, 1144381.64583333, 1144381.64583333, 1144381.64583333, 1144381.64583333, 1144381.64583333, 1144381.64583333, 1144381.64583333, 1144381.64583333, 1144381.64583333, 1229810.48593333, 1229810.48593333, 1229810.48593333, 1229810.48593333, 1229810.48593333, 1229810.48593333, 1229810.48593333, 1229810.48593333, 1229810.48593333, 1229810.48593333, 1229810.48593333, 1229810.48593333], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#ab63fa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Compliance', 
    hovertextsrc: 'sumon23:120:7b8eb0', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Compliance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace75 = {
    mode: 'markers', 
    name: 'Description=Convayance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:9662d7', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:a58706', 
    y: [72750, 43650, 72750, 72750, 72750, 72750, 72750, 72750, 72750, 58200, 43650, 72750, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 52922.8, 52922.8, 52922.8, 52922.8, 52922.8, 52922.8, 52922.8, 52922.8, 52922.8, 52922.8, 52922.8, 52922.8, 61615.625, 61615.625, 61615.625, 61615.625, 61615.625, 61615.625, 61615.625, 12323.125, 61615.625, 61615.625, 61615.625, 61615.625], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FFA15A', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Convayance', 
    hovertextsrc: 'sumon23:120:2ec3f8', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Convayance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace76 = {
    mode: 'markers', 
    name: 'Description=Casual Labour', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:6bd7ed', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:d5e309', 
    y: [210000, 210000, 210000, 210000, 210000, 210000, 210000, 210000, 210000, 210000, 210000, 210000, 42000, 42000, 42000, 42000, 42000, 42000, 42000, 42000, 42000, 42000, 42000, 42000, 802000, 802000, 802000, 802000, 802000, 802000, 802000, 802000, 802000, 802000, 802000, 802000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#19d3f3', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Casual Labour', 
    hovertextsrc: 'sumon23:120:f53fc1', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Casual Labour<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace77 = {
    mode: 'markers', 
    name: 'Description=Mobile Allowance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:cbbdea', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:f53de1', 
    y: [126000, 126000, 126000, 126000, 126000, 126000, 126000, 126000, 126000, 126000, 126000, 126000, 2100, 2100, 2100, 2100, 2100, 2100, 2100, 2100, 2100, 2100, 2100, 2100], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF6692', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Mobile Allowance', 
    hovertextsrc: 'sumon23:120:7382d5', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Mobile Allowance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace78 = {
    mode: 'markers', 
    name: 'Description=Facility Expeses', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:5bff24', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:f9d754', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 47010, 47010, 47010, 47010, 47010, 47010, 47010, 47010, 47010, 47010, 47010, 47010, 75000, 75000, 75000, 75000, 75000, 75000, 75000, 75000, 225000, 225000, 75000, 75000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#B6E880', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Facility Expeses', 
    hovertextsrc: 'sumon23:120:0bece6', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Facility Expeses<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace79 = {
    mode: 'markers', 
    name: 'Description=Legal Expenses', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:e700b4', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:b27a11', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 65152.0833333333, 65152.0833333333, 65152.0833333333, 65152.0833333333, 65152.0833333333, 65152.0833333333, 65152.0833333333, 65152.0833333333, 65152.0833333333, 65152.0833333333, 65152.0833333333, 65152.0833333333], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF97FF', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Legal Expenses', 
    hovertextsrc: 'sumon23:120:4daefc', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Legal Expenses<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace80 = {
    mode: 'markers', 
    name: 'Description=Washing Charge', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:3f7da6', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:ae2fc4', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 70495.9410790425, 73577.9027633131, 74520.6450522933, 78146.5342128796, 60200.2269944661, 29075.2920620521, 62286.0986439324, 46783.6409541974, 3597203.12301931, 2077760.79770928, 4549019.69683021, 4835894.00896865, 4464452.7175772, 4032589.89200036, 3484822.56944306, 0, 4178749.58307533, 4053168.12041538, 3336006.95191142, 4907767.64792832], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FECB52', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Washing Charge', 
    hovertextsrc: 'sumon23:120:81ad1d', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Washing Charge<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace81 = {
    mode: 'markers', 
    name: 'Description=Accomodation', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:110603', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:d96c27', 
    y: [16800, 16800, 16800, 16800, 16800, 16800, 16800, 16800, 16800, 16800, 16800, 16800, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 75895, 75895, 75895, 75895, 75895, 75895, 75895, 75895, 75895, 75895, 75895, 75895, 300500, 300500, 300500, 300500, 300500, 300500, 300500, 300500, 300500, 300500, 300500, 300500], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#636efa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Accomodation', 
    hovertextsrc: 'sumon23:120:ac2314', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Accomodation<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace82 = {
    mode: 'markers', 
    name: 'Description=Meal', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:48ba03', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:72ecc9', 
    y: [276750, 184500, 266500, 276750, 256250, 266500, 256250, 256250, 256250, 246000, 174250, 266500, 205875, 137250, 198250, 205875, 190625, 198250, 190625, 190625, 190625, 183000, 129625, 198250], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#EF553B', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Meal', 
    hovertextsrc: 'sumon23:120:6274ff', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Meal<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace83 = {
    mode: 'markers', 
    name: 'Description=VAT Expense ', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:561068', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:38f926', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 504000, 504000, 504000, 504000, 504000, 504000, 504000, 504000, 504000, 504000, 504000, 504000, 1050000, 1050000, 1050000, 1050000, 1050000, 1050000, 1050000, 1050000, 1050000, 1050000, 1050000, 1050000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#00cc96', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=VAT Expense ', 
    hovertextsrc: 'sumon23:120:aeb905', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=VAT Expense <br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace84 = {
    mode: 'markers', 
    name: 'Description=BSC Common', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:6ac3ff', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:921a87', 
    y: [8607986.60476625, 8607986.60476625, 8607986.60476625, 8607986.60476625, 8607986.60476625, 8607986.60476625, 8416236.60476625, 8416236.60476625, 8416236.60476625, 8416236.60476625, 8416236.60476625, 8416236.60476625, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5196481.87052486, 5097658.50900636, 4796993.05348522, 4667874.0526299, 4993113.26624445, 5006802.07926944, 5075607.38344534, 15745823.3495136, 4817310.02694198, 4582455.99292789, 4564003.47145913, 4564007.31093064, 10549341.4789887, 10648164.8405072, 10948830.2960284, 11077949.2968837, 10752710.0832691, 10739021.2702441, 10670215.9660682, 0, 10928513.3225716, 11163367.3565857, 11181819.8780545, 11181816.0385829], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#ab63fa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=BSC Common', 
    hovertextsrc: 'sumon23:120:c3ad0f', 
    hovertext: ['BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common', 'BSC Common'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=BSC Common<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace85 = {
    mode: 'markers', 
    name: 'Description=Q Collection Expenses (Singapore)', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:ea0c3d', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:bd0bb5', 
    y: [7542638.22161732, 7992605.73721689, 8259132.87861216, 8398021.42547909, 8266939.13907942, 8249411.63557159, 8464358.37926181, 8048972.03593794, 8737907.74420371, 9033845.10426946, 8724980.13052805, 9346054.27171425, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6383497.35697776, 6551780.54101973, 6384291.65256811, 6411279.50891445, 6775473.1665179, 6789773.08159736, 7368504.39222547, 21102504.0478758, 7257292.03750336, 7134606.3163587, 6855436.63068271, 7495613.11101102, 12959093.3109863, 13685585.0732936, 14571737.9793854, 15215455.3715708, 14591036.6442246, 14563291.3762081, 15490467.8932432, 0, 16463838.1740182, 17380686.5525925, 16795836.8281441, 18364249.0455663], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FFA15A', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Q Collection Expenses (Singapore)', 
    hovertextsrc: 'sumon23:120:7f5343', 
    hovertext: ['Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Q Collection Expenses (Singapore)<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace86 = {
    mode: 'markers', 
    name: 'Description=Q Collection Expenses (Bangladesh)', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:f042c6', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:410b6d', 
    y: [2739717.14210526, 2739717.14210526, 2739717.14210526, 2739717.14210526, 2739717.14210526, 2739717.14210526, 2739717.14210526, 2739717.14210526, 2739717.14210526, 2739717.14210526, 2739717.14210526, 2739717.14210526, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1679171.60237042, 1647238.19310484, 1550082.28891733, 1508359.26072926, 1613455.83666009, 1617879.1882034, 1640112.67534784, 5088046.12105263, 1556647.43928728, 1480757.59026164, 1474794.91189298, 1474796.15256536, 3408874.51868221, 3440807.92794779, 3537963.83213531, 3579686.86032337, 3474590.28439254, 3470166.93284924, 3447933.4457048, 0, 3531398.68176536, 3607288.53079099, 3613251.20915965, 3613249.96848727], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#19d3f3', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Q Collection Expenses (Bangladesh)', 
    hovertextsrc: 'sumon23:120:310f48', 
    hovertext: ['Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Q Collection Expenses (Bangladesh)<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace87 = {
    mode: 'markers', 
    name: 'Description=UK Office Expense ', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:f68a55', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:322334', 
    y: [1587468.38433154, 1587468.38433154, 1587468.38433154, 1587468.38433154, 1587468.38433154, 1587468.38433154, 1587468.38433154, 1587468.38433154, 1587468.38433154, 1587468.38433154, 1587468.38433154, 1587468.38433154, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 972958.773613414, 954455.667276649, 898160.831624269, 873985.347546289, 934881.229470619, 937444.242498445, 950326.943917804, 2948155.57090144, 901964.862518739, 857992.171262254, 854537.229417818, 854537.948297935, 1975196.79728802, 1993699.90362479, 2049994.73927717, 2074170.22335515, 2013274.34143082, 2010711.32840299, 1997828.62698363, 0, 2046190.7083827, 2090163.39963918, 2093618.34148362, 2093617.6226035], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF6692', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=UK Office Expense ', 
    hovertextsrc: 'sumon23:120:b00a9a', 
    hovertext: ['Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office', 'Q Collection Expenses & UK Office'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=UK Office Expense <br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace88 = {
    mode: 'markers', 
    name: 'Description=Birichina CHO Direct ', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:afb033', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:b26465', 
    y: [587416.666666667, 587416.666666667, 587416.666666667, 587416.666666667, 587416.666666667, 587416.666666667, 587416.666666667, 587416.666666667, 587416.666666667, 587416.666666667, 587416.666666667, 587416.666666667, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#B6E880', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Birichina CHO Direct ', 
    hovertextsrc: 'sumon23:120:dca1d2', 
    hovertext: ['Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct ', 'Birichina CHO Direct '], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Birichina CHO Direct <br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace89 = {
    mode: 'markers', 
    name: 'Description=C& F Charges - Inward', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:2a341b', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:03a727', 
    y: [1698785.599, 1698785.599, 1698785.599, 1698785.599, 1698785.599, 1698785.599, 1698785.599, 1698785.599, 1698785.599, 1698785.599, 1698785.599, 1698785.599, 728050.971, 728050.971, 728050.971, 728050.971, 728050.971, 728050.971, 728050.971, 728050.971, 728050.971, 728050.971, 728050.971, 728050.971, 493758.277103692, 484368.29866506, 455799.728428189, 443531.124978364, 474434.639656586, 475735.32056017, 482273.049218128, 1496133.5, 457730.202556633, 435414.8888319, 433661.570830288, 433661.935648426, 1002375.22289631, 1011765.20133494, 1040333.77157181, 1052602.37502164, 1021698.86034341, 1020398.17943983, 1013860.45078187, 0, 1038403.29744337, 1060718.6111681, 1062471.92916971, 1062471.56435157], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF97FF', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=C& F Charges - Inward', 
    hovertextsrc: 'sumon23:120:1de855', 
    hovertext: ['Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=C& F Charges - Inward<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace90 = {
    mode: 'markers', 
    name: 'Description=C& F Charges - Outward', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:2ca76b', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:3d5488', 
    y: [1677739.81850432, 1587722.62816896, 1299648.33739565, 2076128.47117164, 1617002.18682339, 2333067.02849418, 2354723.38202931, 2748608.26003403, 2886152.93322663, 1763933.93967844, 1299424.83242869, 1785380.76199099, 719031.350787566, 680452.554929554, 556992.144598134, 889769.344787845, 693000.937210026, 999885.86935465, 1009167.16372685, 1177974.96858601, 1236922.68566856, 755971.688433617, 556896.356755153, 765163.183710423, 678679.802839185, 883305.764504294, 899414.096500405, 977294.681412369, 905673.967534534, 958047.607126306, 1035468.41481008, 1149459.8160657, 773254.601967319, 936151.649809413, 702874.207398739, 995995.584258069, 1377783.11816186, 1845079.53374957, 2052855.23632017, 2319347.26745694, 1950376.26498461, 2054903.20327125, 2176817.62543384, 0, 1754199.57861919, 2280568.49524025, 1722043.55960981, 2440188.77264763], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FECB52', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=C& F Charges - Outward', 
    hovertextsrc: 'sumon23:120:497b1a', 
    hovertext: ['Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=C& F Charges - Outward<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace91 = {
    mode: 'markers', 
    name: 'Description=Transport - Inward', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:fdb698', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:90835d', 
    y: [1012200, 1012200, 1012200, 1012200, 1012200, 1012200, 1012200, 1012200, 1012200, 1012200, 1012200, 1012200, 433800, 433800, 433800, 433800, 433800, 433800, 433800, 433800, 433800, 433800, 433800, 433800, 624898.311344436, 613014.395789073, 576858.138514228, 561331.047761802, 600442.400487487, 602088.536538138, 610362.65727258, 1893500, 579301.338109857, 551059.174868554, 548840.183290563, 548840.645002799, 1268601.68865556, 1280485.60421093, 1316641.86148577, 1332168.9522382, 1293057.59951251, 1291411.46346186, 1283137.34272742, 0, 1314198.66189014, 1342440.82513145, 1344659.81670944, 1344659.3549972], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#636efa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Transport - Inward', 
    hovertextsrc: 'sumon23:120:bf0e74', 
    hovertext: ['Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Transport - Inward<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace92 = {
    mode: 'markers', 
    name: 'Description=Transport - Outward', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:464e76', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:0f1322', 
    y: [1089403.20614105, 1036164.4651338, 1017666.27571308, 1326795.57761849, 1241431.92719414, 1272876.37659969, 1332495.57022356, 1300476.60897125, 1419772.81453305, 1434657.40866167, 1056859.0018641, 1452100.71639039, 466887.088346166, 444070.485057343, 436142.68959132, 568626.676122212, 532042.254511775, 545518.447114151, 571069.530095812, 557347.118130534, 608474.063371309, 614853.175140714, 452939.572227471, 622328.878453023, 1117220.6379875, 900661.216714062, 1343206.36484148, 1326222.35659685, 1232391.18675497, 1062174.08431979, 916542.237073173, 1149345.97633447, 1252774.22876258, 1191063.67820593, 890803.588707002, 1261972.10707289, 2268061.79857091, 1881332.20067197, 3065782.74707102, 3147433.68332658, 2653964.4573934, 2278242.65940961, 1926804.59160251, 0, 2842034.20013165, 2901562.26386442, 2182471.01211378, 3091831.14437969], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#EF553B', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Transport - Outward', 
    hovertextsrc: 'sumon23:120:62f44e', 
    hovertext: ['Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Transport - Outward<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace93 = {
    mode: 'markers', 
    name: 'Description=Documentation Chrages & Misc Expenses', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:b1b21a', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:8a10a1', 
    y: [1259409.88819286, 1194264.33560202, 1026569.08647076, 1573004.07831, 1249111.76982545, 1780836.23120121, 1767465.45648945, 2040596.61406134, 2141845.54093323, 1360231.12490562, 1019426.95675037, 1389966.37384633, 539747.094939797, 511827.572400865, 439958.179916039, 674144.60499, 535333.615639479, 763215.527657663, 757485.195638336, 874541.406026289, 917933.803257099, 582956.196388122, 436897.26717873, 595699.874505569, 346040.048107684, 486333.823548411, 500397.224261824, 539200.711515193, 502635.133444161, 526880.568389034, 563952.732913928, 672039.696803797, 434794.914950447, 515699.182249713, 392011.326955426, 547306.126697075, 702493.479983012, 1015870.85747473, 1142124.70769909, 1279648.52428775, 1082428.85338259, 1130098.92167213, 1185571.89322262, 0, 986372.476350392, 1256299.98974983, 960428.727888611, 1340899.78577799], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#00cc96', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Documentation Chrages & Misc Expenses', 
    hovertextsrc: 'sumon23:120:aa639f', 
    hovertext: ['Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)', 'Logistic Expenses (Outward)'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Documentation Chrages & Misc Expenses<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace94 = {
    mode: 'markers', 
    name: 'Description=Custom & VAT service charges', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:9b5f80', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:431462', 
    y: [0, 0, 0, 0, 0, 280000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 120000, 0, 0, 0, 0, 0, 0, 11880.8234530762, 11654.8815676824, 14775.6111528598, 10672.2565193688, 11415.8576274357, 184585.368608603, 11604.4656254623, 36000, 11013.9150630868, 10476.9634514222, 10434.7750718037, 57536.2387288384, 24119.1765469238, 24345.1184323176, 33724.3888471402, 25327.7434806312, 24584.1423725643, 395914.631391397, 24395.5343745377, 0, 24986.0849369132, 25523.0365485778, 25565.2249281963, 140963.761271162], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#ab63fa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Custom & VAT service charges', 
    hovertextsrc: 'sumon23:120:e24f0a', 
    hovertext: ['Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Custom & VAT service charges<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace95 = {
    mode: 'markers', 
    name: 'Description=Marine Insurance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:534e87', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:fb21a4', 
    y: [432343.231234687, 432343.231234687, 432343.231234687, 432343.231234687, 432343.231234687, 432343.231234687, 432343.231234687, 432343.231234687, 432343.231234687, 432343.231234687, 432343.231234687, 432343.231234687, 185289.956243437, 185289.956243437, 185289.956243437, 185289.956243437, 185289.956243437, 185289.956243437, 185289.956243437, 185289.956243437, 185289.956243437, 185289.956243437, 185289.956243437, 185289.956243437, 87571.0468099923, 85905.6767705923, 80838.8663138996, 78662.9545432656, 84143.8816607902, 84374.5653648909, 85534.0714813156, 265348.416093455, 81181.2476938312, 77223.4904806637, 76912.5288220713, 76912.5935247967, 177777.369283462, 179442.739322862, 184509.549779555, 186685.461550189, 181204.534432664, 180973.850728564, 179814.344612139, 0, 184167.168399623, 188124.925612791, 188435.887271383, 188435.822568658], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FFA15A', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Marine Insurance', 
    hovertextsrc: 'sumon23:120:3f9088', 
    hovertext: ['Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)', 'Logistic Expenses (Inward)'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Marine Insurance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace96 = {
    mode: 'markers', 
    name: 'Description=Air Freight', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:130f56', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:19e755', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#19d3f3', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Air Freight', 
    hovertextsrc: 'sumon23:120:a78e2e', 
    hovertext: ['Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight', 'Air Freight'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Air Freight<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace97 = {
    mode: 'markers', 
    name: 'Description=Discount', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:09eb77', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:2f7ee9', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF6692', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Discount', 
    hovertextsrc: 'sumon23:120:662713', 
    hovertext: ['Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Discount<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace98 = {
    mode: 'markers', 
    name: 'Description=Claims', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:2b3049', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:399a1d', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#B6E880', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Claims', 
    hovertextsrc: 'sumon23:120:09f1ef', 
    hovertext: ['Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims', 'Claims'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Claims<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace99 = {
    mode: 'markers', 
    name: 'Description=Inspection charges', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:662706', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:ee5bf8', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF97FF', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Inspection charges', 
    hovertextsrc: 'sumon23:120:6d17ff', 
    hovertext: ['Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges', 'Inspection charges'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Inspection charges<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace100 = {
    mode: 'markers', 
    name: 'Description=Demmurage Expense ', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:aa9f1c', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:c5acd5', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FECB52', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Demmurage Expense ', 
    hovertextsrc: 'sumon23:120:a1374c', 
    hovertext: ['Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount', 'Discount'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Demmurage Expense <br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace101 = {
    mode: 'markers', 
    name: 'Description=Interest - Term Loan', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:1a69a1', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:b210e5', 
    y: [26345317.4533711, 26286344.78057, 26443688.2500088, 26750235.1744672, 27043710.34799, 27339839.0407889, 28188645.2100924, 28124326.6401811, 28059380.7024324, 27993801.2681649, 27927582.1487396, 27860717.0949725, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7584511.23210952, 7263509.74746615, 6997360.17711525, 6966666.96498879, 7619195.85308483, 7806122.6827214, 8080141.4534435, 24495659.4297901, 7318034.95057534, 9431655.53572762, 9059401.97906665, 8955543.12658645, 15397263.1738764, 15172269.5772978, 15971027.7345011, 16533518.8002639, 16408000.321108, 16743245.7286413, 16986509.7575964, 0, 16601639.0900993, 22976551.3708359, 22195557.4237167, 21941076.9845297], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#636efa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Interest - Term Loan', 
    hovertextsrc: 'sumon23:120:cb09d8', 
    hovertext: ['Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan', 'Interest - Term Loan'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Interest - Term Loan<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace102 = {
    mode: 'markers', 
    name: 'Description=Interest - WC', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:be41cd', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:b2f69c', 
    y: [10453741.5853845, 10025464.8528619, 9795521.7551532, 9667393.299376, 9306132.17215348, 9156697.64801144, 9592902.12776383, 8902775.65939242, 9464147.20163676, 9436113.66565703, 9120015.19634, 9528458.9009511, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3530378.7887179, 3462583.09143366, 3257731.78654968, 3169431.15202398, 3389602.29974493, 3347436.50457805, 3341266.00287915, 10203584.0360386, 3072176.07256655, 2875279.92524695, 2816764.4005748, 2816119.47014271, 7166997.27244541, 7232762.93741485, 7435564.75550766, 7521796.26232712, 7299536.15775942, 7179870.75468753, 7024189.84712751, 0, 6969515.54924982, 7004498.48467384, 6901079.80046603, 6899491.54603491], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#EF553B', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Interest - WC', 
    hovertextsrc: 'sumon23:120:8dfa38', 
    hovertext: ['Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC', 'Interest - WC'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Interest - WC<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace103 = {
    mode: 'markers', 
    name: 'Description=Bank Charge', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:535772', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:debb45', 
    y: [5776611.71136482, 5573373.3770077, 6476297.81245559, 9004041.58579832, 8325392.37272467, 8554809.5313783, 8943367.22219713, 8644887.35672514, 8736023.30946739, 8784806.16312224, 7044055.12975788, 9252103.69135721, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 676817.173598667, 2006924.04376777, 2064596.14240516, 2403585.82444489, 3173057.79829852, 2575717.0080632, 1913241.65538662, 3278053.26626029, 1136788.92422352, 2147059.10071133, 3195355.68500536, 3848249.83961438, 1374001.80757586, 4192132.13334332, 4712308.84451781, 5704267.42317678, 6833205.81033459, 5524620.16628314, 4022119.9387561, 0, 2578910.80994323, 5230472.37431883, 7828629.3907275, 9428210.52762515], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#00cc96', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Bank Charge', 
    hovertextsrc: 'sumon23:120:321a73', 
    hovertext: ['Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge', 'Bank Charge'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Bank Charge<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace104 = {
    mode: 'markers', 
    name: 'Description=Interest on IDBP', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:b67773', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:3a1d2a', 
    y: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#ab63fa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Interest on IDBP', 
    hovertextsrc: 'sumon23:120:acbf4e', 
    hovertext: ['Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP', 'Interest on IDBP'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Interest on IDBP<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace105 = {
    mode: 'markers', 
    name: 'Description=Attendence Bonus', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:5786fa', 
    x: ['2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:55df64', 
    y: [1146600, 0, 0, 1146600, 0, 0, 1146600, 0, 0, 1146600, 0, 0, 1146600, 0, 0, 1146600, 0, 0, 1146600, 0, 0, 1146600, 0, 0, 1146600, 0, 0, 1146600, 0, 0, 1146600, 0, 0, 678672, 0, 0, 678672, 0, 0, 678672, 0, 0, 678672, 0, 0, 678672, 0, 0, 678672, 0, 0, 678672, 0, 0, 678672, 0, 0, 678672, 0, 0, 678672, 0, 0, 678672, 0, 0, 678672, 0, 0, 810112.461440678, 810112.461440678, 810112.461440678, 810112.461440678, 810112.461440678, 810112.461440678, 810112.461440678, 810112.461440678, 810112.461440678, 810112.461440678, 810112.461440678, 810112.461440678, 1282875.94285714, 1287031.42857143, 1313963.54285714, 1313963.54285714, 1313963.54285714, 1313963.54285714, 1313963.54285714, 1313963.54285714, 1333820.11428571, 1457190.11428571, 1457190.11428571], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FFA15A', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Attendence Bonus', 
    hovertextsrc: 'sumon23:120:da737d', 
    hovertext: ['Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Salaries', 'Salaries', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Attendence Bonus<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace106 = {
    mode: 'markers', 
    name: 'Description=Additonal Allowance', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:a2b5ac', 
    x: ['2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:b8a263', 
    y: [235200, 0, 235200, 0, 235200, 0, 235200, 0, 235200, 0, 235200, 0, 235200, 0, 235200, 0, 235200, 0, 235200, 0, 235200, 0, 84000, 0, 84000, 0, 84000, 0, 84000, 0, 84000, 0, 84000, 0, 84000, 0, 84000, 0, 84000, 0, 84000, 0, 84000, 0, 84000, 0], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#19d3f3', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Additonal Allowance', 
    hovertextsrc: 'sumon23:120:1b8aaf', 
    hovertext: ['Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries', 'Direct Wages', 'Salaries'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Additonal Allowance<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace107 = {
    mode: 'markers', 
    name: 'Description=Subcontract', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:00f824', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:d5847d', 
    y: [1260000, 1260000, 1260000, 1260000, 1260000, 1260000, 1260000, 1260000, 1260000, 2100000, 2100000, 2100000, 2100000, 2100000, 2100000, 2100000, 2100000, 2100000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF6692', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Subcontract', 
    hovertextsrc: 'sumon23:120:750731', 
    hovertext: ['Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Subcontract<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace108 = {
    mode: 'markers', 
    name: 'Description=Provision for Resignation Benefit', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:175834', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:616bfa', 
    y: [856758.9, 856758.9, 856758.9, 856758.9, 856758.9, 856758.9, 856758.9, 856758.9, 856758.9, 856758.9, 856758.9, 856758.9, 535000, 535000, 535000, 535000, 535000, 535000, 535000, 535000, 535000, 535000, 535000, 535000], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#B6E880', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Provision for Resignation Benefit', 
    hovertextsrc: 'sumon23:120:16f44c', 
    hovertext: ['Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Provision for Resignation Benefit<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace109 = {
    mode: 'markers', 
    name: 'Description=HR Event & activities', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:0c9806', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:33882c', 
    y: [436316.666666667, 436316.666666667, 436316.666666667, 436316.666666667, 436316.666666667, 436316.666666667, 436316.666666667, 436316.666666667, 436316.666666667, 436316.666666667, 436316.666666667, 436316.666666667, 720020, 720020, 720020, 720020, 720020, 720020, 720020, 720020, 720020, 720020, 720020, 720020], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FF97FF', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=HR Event & activities', 
    hovertextsrc: 'sumon23:120:82a826', 
    hovertext: ['Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses', 'Other Operation Expenses'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=HR Event & activities<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace110 = {
    mode: 'markers', 
    name: 'Description=Interest - WC(QCPL)', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:f7211b', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:a11773', 
    y: [404048.917905421, 415505.20019154, 459144.2955772, 569151.797061948, 603484.937736964, 611214.586929408, 976361.510345334, 2043884.95181965, 1035020.15068679, 1055735.60698053, 967306.767568972, 1273152.63007536, 820256.880597881, 867921.587118999, 1047967.5324966, 1350729.4067907, 1299609.72825799, 1310985.8040716, 2052559.89860853, 0, 2348038.93514219, 2571888.18187022, 2369903.99096286, 3119223.42114693], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FECB52', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Interest - WC(QCPL)', 
    hovertextsrc: 'sumon23:120:dc0e6f', 
    hovertext: ['Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)', 'Interest - WC(QCPL)'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Interest - WC(QCPL)<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace111 = {
    mode: 'markers', 
    name: 'Description=Interest - Term Loan(QCPL)', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:9a5927', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:18977c', 
    y: [1356568.31979069, 1330769.97298001, 1252279.70937899, 1218572.53009739, 1303477.89964091, 1307051.43468788, 1325013.41323494, 4110528.17217144, 1257583.55979783, 1196273.70627449, 1191456.5806367, 1191457.58295049, 2753959.85238076, 2779758.19919144, 2858248.46279246, 2891955.64207406, 2807050.27253053, 2803476.73748357, 2785514.7589365, 0, 2852944.61237362, 2914254.46589696, 2919071.59153474, 2919070.58922095], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#636efa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Interest - Term Loan(QCPL)', 
    hovertextsrc: 'sumon23:120:849de2', 
    hovertext: ['Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)', 'Interest - Term Loan(QCPL)'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Interest - Term Loan(QCPL)<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace112 = {
    mode: 'markers', 
    name: 'Description=Interest on EDF', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:e6e36a', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:31c5fc', 
    y: [2021390.10139143, 1982948.60005708, 1865992.1301292, 1815765.86614261, 1942281.33244566, 1947606.1717962, 1974370.88766546, 6125000, 1873895.27115018, 1782538.92055447, 1775361.03652215, 1775362.53004602, 4103609.89860857, 4142051.39994292, 4259007.8698708, 4309234.13385739, 4182718.66755434, 4177393.8282038, 4150629.11233454, 0, 4251104.72884982, 4342461.07944553, 4349638.96347785, 4349637.46995398], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#EF553B', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Interest on EDF', 
    hovertextsrc: 'sumon23:120:0df330', 
    hovertext: ['Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF', 'Interest on EDF'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Interest on EDF<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace113 = {
    mode: 'markers', 
    name: 'Description=Depreciation', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:289c86', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00', '2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:ec7c00', 
    y: [9276963.99999999, 9276963.99999999, 9276963.99999999, 9276963.99999999, 9276963.99999999, 9276963.99999999, 9276963.99999999, 9276963.99999999, 9276963.99999999, 9276963.99999999, 9276963.99999999, 9276963.99999999, 31270053.25, 31270053.25, 31270053.25, 31270053.25, 31270053.25, 31270053.25, 31270053.25, 31270053.25, 31270053.25, 31270053.25, 31270053.25, 31270053.25], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#00cc96', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Depreciation', 
    hovertextsrc: 'sumon23:120:7de8ae', 
    hovertext: ['Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation', 'Depreciation'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Depreciation<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace114 = {
    mode: 'markers', 
    name: 'Description=Winding Wages', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:46af01', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:a9bd79', 
    y: [1908732, 1908732, 1908732, 1908732, 1908732, 1908732, 1908732, 1908732, 1908732, 1908732, 1908732, 1908732], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#ab63fa', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Winding Wages', 
    hovertextsrc: 'sumon23:120:87ab72', 
    hovertext: ['Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages', 'Direct Wages'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Winding Wages<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace115 = {
    mode: 'markers', 
    name: 'Description=Winding Utility', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:8c2bd0', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:81408b', 
    y: [446292, 446292, 446292, 446292, 446292, 446292, 446292, 446292, 446292, 446292, 446292, 446292], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#FFA15A', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Winding Utility', 
    hovertextsrc: 'sumon23:120:1a6319', 
    hovertext: ['Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility', 'Utility'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Winding Utility<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  trace116 = {
    mode: 'markers', 
    name: 'Description=Winding  Consumable', 
    type: 'scattergl', 
    xsrc: 'sumon23:120:8e8f19', 
    x: ['2019-07-01T00:00:00', '2019-08-01T00:00:00', '2019-09-01T00:00:00', '2019-10-01T00:00:00', '2019-11-01T00:00:00', '2019-12-01T00:00:00', '2020-01-01T00:00:00', '2020-02-01T00:00:00', '2020-03-01T00:00:00', '2020-04-01T00:00:00', '2020-05-01T00:00:00', '2020-06-01T00:00:00'], 
    ysrc: 'sumon23:120:a49172', 
    y: [1398936, 1398936, 1398936, 1398936, 1398936, 1398936, 1398936, 1398936, 1398936, 1398936, 1398936, 1398936], 
    xaxis: 'x', 
    yaxis: 'y', 
    marker: {
      color: '#19d3f3', 
      symbol: 'circle'
    }, 
    hoverlabel: {namelength: 0}, 
    showlegend: true, 
    legendgroup: 'Description=Winding  Consumable', 
    hovertextsrc: 'sumon23:120:3c18b6', 
    hovertext: ['Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables', 'Needles & Consumables'], 
    hovertemplate: '<b>%{hovertext}</b><br><br>Description=Winding  Consumable<br>Month=%{x}<br>Amount BDT=%{y}'
  };
  export const dataArea = [trace1, trace2, trace3, trace4, trace5, trace6, trace7, trace8, trace9, trace10, trace11, trace12, trace13, trace14, trace15, trace16, trace17, trace18, trace19, trace20, trace21, trace22, trace23, trace24, trace25, trace26, trace27, trace28, trace29, trace30, trace31, trace32, trace33, trace34, trace35, trace36, trace37, trace38, trace39, trace40, trace41, trace42, trace43, trace44, trace45, trace46, trace47, trace48, trace49, trace50, trace51, trace52, trace53, trace54, trace55, trace56, trace57, trace58, trace59, trace60, trace61, trace62, trace63, trace64, trace65, trace66, trace67, trace68, trace69, trace70, trace71, trace72, trace73, trace74, trace75, trace76, trace77, trace78, trace79, trace80, trace81, trace82, trace83, trace84, trace85, trace86, trace87, trace88, trace89, trace90, trace91, trace92, trace93, trace94, trace95, trace96, trace97, trace98, trace99, trace100, trace101, trace102, trace103, trace104, trace105, trace106, trace107, trace108, trace109, trace110, trace111, trace112, trace113, trace114, trace115, trace116];
  export const layoutArea = {
    title: {text: 'Description of BDT expenses'}, 
    xaxis: {
      title: {text: 'Month'}, 
      anchor: 'y', 
      domain: [0.0, 0.98]
    }, 
    yaxis: {
      title: {text: 'Amount BDT'}, 
      anchor: 'x', 
      domain: [0.0, 1.0]
    }, 
    height: 600, 
    legend: {tracegroupgap: 0}, 
    template: {
      data: {
        bar: [
          {
            type: 'bar', 
            marker: {line: {
                color: '#E5ECF6', 
                width: 0.5
              }}, 
            error_x: {color: '#2a3f5f'}, 
            error_y: {color: '#2a3f5f'}
          }
        ], 
        table: [
          {
            type: 'table', 
            cells: {
              fill: {color: '#EBF0F8'}, 
              line: {color: 'white'}
            }, 
            header: {
              fill: {color: '#C8D4E3'}, 
              line: {color: 'white'}
            }
          }
        ], 
        carpet: [
          {
            type: 'carpet', 
            aaxis: {
              gridcolor: 'white', 
              linecolor: 'white', 
              endlinecolor: '#2a3f5f', 
              minorgridcolor: 'white', 
              startlinecolor: '#2a3f5f'
            }, 
            baxis: {
              gridcolor: 'white', 
              linecolor: 'white', 
              endlinecolor: '#2a3f5f', 
              minorgridcolor: 'white', 
              startlinecolor: '#2a3f5f'
            }
          }
        ], 
        mesh3d: [
          {
            type: 'mesh3d', 
            colorbar: {
              ticks: '', 
              outlinewidth: 0
            }
          }
        ], 
        contour: [
          {
            type: 'contour', 
            colorbar: {
              ticks: '', 
              outlinewidth: 0
            }, 
            colorscale: [['0.0', '#0d0887'], ['0.1111111111111111', '#46039f'], ['0.2222222222222222', '#7201a8'], ['0.3333333333333333', '#9c179e'], ['0.4444444444444444', '#bd3786'], ['0.5555555555555556', '#d8576b'], ['0.6666666666666666', '#ed7953'], ['0.7777777777777778', '#fb9f3a'], ['0.8888888888888888', '#fdca26'], ['1.0', '#f0f921']]
          }
        ], 
        heatmap: [
          {
            type: 'heatmap', 
            colorbar: {
              ticks: '', 
              outlinewidth: 0
            }, 
            colorscale: [['0.0', '#0d0887'], ['0.1111111111111111', '#46039f'], ['0.2222222222222222', '#7201a8'], ['0.3333333333333333', '#9c179e'], ['0.4444444444444444', '#bd3786'], ['0.5555555555555556', '#d8576b'], ['0.6666666666666666', '#ed7953'], ['0.7777777777777778', '#fb9f3a'], ['0.8888888888888888', '#fdca26'], ['1.0', '#f0f921']]
          }
        ], 
        scatter: [
          {
            type: 'scatter', 
            marker: {colorbar: {
                ticks: '', 
                outlinewidth: 0
              }}
          }
        ], 
        surface: [
          {
            type: 'surface', 
            colorbar: {
              ticks: '', 
              outlinewidth: 0
            }, 
            colorscale: [['0.0', '#0d0887'], ['0.1111111111111111', '#46039f'], ['0.2222222222222222', '#7201a8'], ['0.3333333333333333', '#9c179e'], ['0.4444444444444444', '#bd3786'], ['0.5555555555555556', '#d8576b'], ['0.6666666666666666', '#ed7953'], ['0.7777777777777778', '#fb9f3a'], ['0.8888888888888888', '#fdca26'], ['1.0', '#f0f921']]
          }
        ], 
        barpolar: [
          {
            type: 'barpolar', 
            marker: {line: {
                color: '#E5ECF6', 
                width: 0.5
              }}
          }
        ], 
        heatmapgl: [
          {
            type: 'heatmapgl', 
            colorbar: {
              ticks: '', 
              outlinewidth: 0
            }, 
            colorscale: [['0.0', '#0d0887'], ['0.1111111111111111', '#46039f'], ['0.2222222222222222', '#7201a8'], ['0.3333333333333333', '#9c179e'], ['0.4444444444444444', '#bd3786'], ['0.5555555555555556', '#d8576b'], ['0.6666666666666666', '#ed7953'], ['0.7777777777777778', '#fb9f3a'], ['0.8888888888888888', '#fdca26'], ['1.0', '#f0f921']]
          }
        ], 
        histogram: [
          {
            type: 'histogram', 
            marker: {colorbar: {
                ticks: '', 
                outlinewidth: 0
              }}
          }
        ], 
        parcoords: [
          {
            line: {colorbar: {
                ticks: '', 
                outlinewidth: 0
              }}, 
            type: 'parcoords'
          }
        ], 
        scatter3d: [
          {
            line: {colorbar: {
                ticks: '', 
                outlinewidth: 0
              }}, 
            type: 'scatter3d', 
            marker: {colorbar: {
                ticks: '', 
                outlinewidth: 0
              }}
          }
        ], 
        scattergl: [
          {
            type: 'scattergl', 
            marker: {colorbar: {
                ticks: '', 
                outlinewidth: 0
              }}
          }
        ], 
        choropleth: [
          {
            type: 'choropleth', 
            colorbar: {
              ticks: '', 
              outlinewidth: 0
            }
          }
        ], 
        scattergeo: [
          {
            type: 'scattergeo', 
            marker: {colorbar: {
                ticks: '', 
                outlinewidth: 0
              }}
          }
        ], 
        histogram2d: [
          {
            type: 'histogram2d', 
            colorbar: {
              ticks: '', 
              outlinewidth: 0
            }, 
            colorscale: [['0.0', '#0d0887'], ['0.1111111111111111', '#46039f'], ['0.2222222222222222', '#7201a8'], ['0.3333333333333333', '#9c179e'], ['0.4444444444444444', '#bd3786'], ['0.5555555555555556', '#d8576b'], ['0.6666666666666666', '#ed7953'], ['0.7777777777777778', '#fb9f3a'], ['0.8888888888888888', '#fdca26'], ['1.0', '#f0f921']]
          }
        ], 
        scatterpolar: [
          {
            type: 'scatterpolar', 
            marker: {colorbar: {
                ticks: '', 
                outlinewidth: 0
              }}
          }
        ], 
        contourcarpet: [
          {
            type: 'contourcarpet', 
            colorbar: {
              ticks: '', 
              outlinewidth: 0
            }
          }
        ], 
        scattercarpet: [
          {
            type: 'scattercarpet', 
            marker: {colorbar: {
                ticks: '', 
                outlinewidth: 0
              }}
          }
        ], 
        scattermapbox: [
          {
            type: 'scattermapbox', 
            marker: {colorbar: {
                ticks: '', 
                outlinewidth: 0
              }}
          }
        ], 
        scatterpolargl: [
          {
            type: 'scatterpolargl', 
            marker: {colorbar: {
                ticks: '', 
                outlinewidth: 0
              }}
          }
        ], 
        scatterternary: [
          {
            type: 'scatterternary', 
            marker: {colorbar: {
                ticks: '', 
                outlinewidth: 0
              }}
          }
        ], 
        histogram2dcontour: [
          {
            type: 'histogram2dcontour', 
            colorbar: {
              ticks: '', 
              outlinewidth: 0
            }, 
            colorscale: [['0.0', '#0d0887'], ['0.1111111111111111', '#46039f'], ['0.2222222222222222', '#7201a8'], ['0.3333333333333333', '#9c179e'], ['0.4444444444444444', '#bd3786'], ['0.5555555555555556', '#d8576b'], ['0.6666666666666666', '#ed7953'], ['0.7777777777777778', '#fb9f3a'], ['0.8888888888888888', '#fdca26'], ['1.0', '#f0f921']]
          }
        ]
      }, 
      layout: {
        geo: {
          bgcolor: 'white', 
          showland: true, 
          lakecolor: 'white', 
          landcolor: '#E5ECF6', 
          showlakes: true, 
          subunitcolor: 'white'
        }, 
        font: {color: '#2a3f5f'}, 
        polar: {
          bgcolor: '#E5ECF6', 
          radialaxis: {
            ticks: '', 
            gridcolor: 'white', 
            linecolor: 'white'
          }, 
          angularaxis: {
            ticks: '', 
            gridcolor: 'white', 
            linecolor: 'white'
          }
        }, 
        scene: {
          xaxis: {
            ticks: '', 
            gridcolor: 'white', 
            gridwidth: 2, 
            linecolor: 'white', 
            zerolinecolor: 'white', 
            showbackground: true, 
            backgroundcolor: '#E5ECF6'
          }, 
          yaxis: {
            ticks: '', 
            gridcolor: 'white', 
            gridwidth: 2, 
            linecolor: 'white', 
            zerolinecolor: 'white', 
            showbackground: true, 
            backgroundcolor: '#E5ECF6'
          }, 
          zaxis: {
            ticks: '', 
            gridcolor: 'white', 
            gridwidth: 2, 
            linecolor: 'white', 
            zerolinecolor: 'white', 
            showbackground: true, 
            backgroundcolor: '#E5ECF6'
          }
        }, 
        title: {x: 0.05}, 
        xaxis: {
          ticks: '', 
          gridcolor: 'white', 
          linecolor: 'white', 
          automargin: true, 
          zerolinecolor: 'white', 
          zerolinewidth: 2
        }, 
        yaxis: {
          ticks: '', 
          gridcolor: 'white', 
          linecolor: 'white', 
          automargin: true, 
          zerolinecolor: 'white', 
          zerolinewidth: 2
        }, 
        mapbox: {style: 'light'}, 
        ternary: {
          aaxis: {
            ticks: '', 
            gridcolor: 'white', 
            linecolor: 'white'
          }, 
          baxis: {
            ticks: '', 
            gridcolor: 'white', 
            linecolor: 'white'
          }, 
          caxis: {
            ticks: '', 
            gridcolor: 'white', 
            linecolor: 'white'
          }, 
          bgcolor: '#E5ECF6'
        }, 
        colorway: ['#636efa', '#EF553B', '#00cc96', '#ab63fa', '#FFA15A', '#19d3f3', '#FF6692', '#B6E880', '#FF97FF', '#FECB52'], 
        hovermode: 'closest', 
        colorscale: {
          diverging: [['0', '#8e0152'], ['0.1', '#c51b7d'], ['0.2', '#de77ae'], ['0.3', '#f1b6da'], ['0.4', '#fde0ef'], ['0.5', '#f7f7f7'], ['0.6', '#e6f5d0'], ['0.7', '#b8e186'], ['0.8', '#7fbc41'], ['0.9', '#4d9221'], ['1', '#276419']], 
          sequential: [['0.0', '#0d0887'], ['0.1111111111111111', '#46039f'], ['0.2222222222222222', '#7201a8'], ['0.3333333333333333', '#9c179e'], ['0.4444444444444444', '#bd3786'], ['0.5555555555555556', '#d8576b'], ['0.6666666666666666', '#ed7953'], ['0.7777777777777778', '#fb9f3a'], ['0.8888888888888888', '#fdca26'], ['1.0', '#f0f921']], 
          sequentialminus: [['0.0', '#0d0887'], ['0.1111111111111111', '#46039f'], ['0.2222222222222222', '#7201a8'], ['0.3333333333333333', '#9c179e'], ['0.4444444444444444', '#bd3786'], ['0.5555555555555556', '#d8576b'], ['0.6666666666666666', '#ed7953'], ['0.7777777777777778', '#fb9f3a'], ['0.8888888888888888', '#fdca26'], ['1.0', '#f0f921']]
        }, 
        hoverlabel: {align: 'left'}, 
        plot_bgcolor: '#E5ECF6', 
        paper_bgcolor: 'white', 
        shapedefaults: {line: {color: '#2a3f5f'}}, 
        annotationdefaults: {
          arrowhead: 0, 
          arrowcolor: '#2a3f5f', 
          arrowwidth: 1
        }
      }
    }
  }