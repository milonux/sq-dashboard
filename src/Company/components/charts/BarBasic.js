import { Chart, Tooltip, Axis, Bar } from 'viser-react';
import * as React from 'react';

const data = [
  { year: '1951 year', sales: 38 },
  { year: '1952 year', sales: 52 },
  { year: '1956 year', sales: 61 },
  { year: '1957 year', sales: 145 },
  { year: '1958 year', sales: 48 },
  { year: '1959 year', sales: 38 },
  { year: '1960 year', sales: 38 },
  { year: '1962 year', sales: 38 },
];

const scale = [{
  dataKey: 'sales',
  tickInterval: 20,
}];

export default class barBasic extends React.Component {
  render() {
    return (
      <Chart forceFit height={400} data={data} scale={scale}>
        <Tooltip />
        <Axis />
        <Bar position="year*sales" />
      </Chart>
    );
  }
}

