import { Chart, Tooltip, Axis, Point } from 'viser-react';
import * as React from 'react';
import * as $ from 'jquery';

import data from './scatter.json'



export default class Scatter extends React.Component {

  render() {
    return (
      <Chart forceFit height={400} data={data}>
        <Tooltip
          showTitle={false}
          crosshairs= {{ type: 'cross' }}
          itemTpl= {`
            <li data-index={index} style="margin-bottom:4px;">
              <span style="background-color:{color};" class="g2-tooltip-marker"></span>
              {name}<br />{value}
            </li>
          `}
        />
        <Axis />
        <Point
          position="height*weight"
          size={4}
          opacity={0.65}
          tooltip={['gender*height*weight', (gender, height, weight) => {
            return {
              name: gender,
              value: height + '(cm), ' + weight + '(kg)'
            };
          }]}
          shape="circle"
        />
      </Chart>
    );
  }
}





