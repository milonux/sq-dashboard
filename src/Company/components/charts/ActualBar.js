import { Chart, Tooltip, Axis, Interval, Legend, Coord } from 'viser-react';
import * as React from 'react';

/* 1951 year
1952 year
1956 year
1957 year
1958 year
1959 year
1960 year
1962 year */

const data = [{
  city: '1951 year',
  type: 'Actual',
  value: 14500
}, {
  city: '1952 year',
  type: 'Basic',
  value: 8500
}, {
  city: '1956 year',
  type: 'Actual',
  value: 10000
}, {
  city: '1957 year',
  type: 'Basic',
  value: 7000
}, {
  city: '1958 year',
  type: 'Actual',
  value: 9000
}, {
  city: '1959 year',
  type: 'Basic',
  value: 8500
}, {
  city: '1960 year',
  type: 'Actual',
  value: 11000
}, {
  city: '1961 year',
  type: 'Actual',
  value: 6000
}, {
  city: '1962 year',
  type: 'Basic',
  value: 16000
}, {
  city: '1963 year',
  type: 'Actual',
  value: 5000
}, {
  city: '1964 year',
  type: 'Basic',
  value: 6000
}, {
  city: '1965 year',
  type: 'Actual',
  value: 10000
}, {
  city: '1966 year',
  type: 'Basic',
  value: 14000
}];

const scale = [{
  dataKey: 'value',
  max: 20000,
  min: 0.0,
  nice: false,
  alias: '销售额（万）'
}];

const label = {
  textStyle: {
    fill: '#aaaaaa'
  }
}

const tickLine = {
  alignWithLabel: false,
  length: 0
}

const title = {
  offset: 30,
  textStyle: {
    fontSize: 14,
    fontWeight: 300
  }
}

const adjust = [{
  type: 'dodge',
  marginRatio: 0.3
}]

export default class ActualBar extends React.Component {
  render() {
    return (
      <Chart forceFit height={400} data={data} padding={[0, 90, 20, 52]} scale={scale}>
        <Tooltip/>
        <Axis dataKey="city" label={label} tickLine={tickLine}/>
        <Axis dataKey="value" label={label} title={title}/>
        <Legend position='right-bottom'/>
        <Coord direction='LB' type='rect'/>
        <Interval position="city*value" opacity={1} adjust={adjust} color='type'/>
      </Chart>
    );
  }
}

