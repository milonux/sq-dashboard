import React, { Component } from "react";
import { Layout, Menu, Dropdown, Icon, Button, Avatar } from "antd";
import { Link } from "react-router-dom";
import AppRouter from "../../router";
import { NotificationTab } from "../components/Header";

import "./PageLayout.scss";

const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;

const userMenu = (
  <Menu style={{ minWidth: "260px" }} className="dropdown-menuuser">
    <Menu.Item style={{ padding: "0" }}>
      <div className="usermenu-header">
        <div>
          <div>
            <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
          </div>
        </div>
        <div>
          <h6>Jone Deo</h6>
          <span>jonedoe@gmail.com</span>
        </div>
      </div>
    </Menu.Item>
    <Menu.Item>
      <Link to="/account">My Account</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="">Company Settings</Link>
    </Menu.Item>
    <Menu.Item>
      <Link to="">
        <Button type="primary" block>
          Logout
        </Button>
      </Link>
    </Menu.Item>
  </Menu>
);

const messageMenu = (
  <Menu>
    <Menu.Item>Message</Menu.Item>
  </Menu>
);

class PageLayout extends Component {
  state = {
    collapsed: false
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };
  render() {
    return (
      <Layout>
        <Sider trigger={null} collapsible collapsed={this.state.collapsed} width={260} className="mi-sidebar">
          <div className="navbar-logo">
            <Link to="/personal-home">
              <img src="/assets/images/logo-white.png" alt="Logo" />
            </Link>
          </div>
          <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
            <Menu.Item key="1">
              <Link to="/personal-home">
                <Icon type="dashboard" />
                <span>Company Dashboard</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/personal-operations">
                <Icon type="solution" />
                <span>Operations</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/personal-finance">
                <Icon type="transaction" />
                <span>Finance</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="4">
              <Link to="/personal-hr">
                <Icon type="audit" />
                <span>HR</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="5">
              <Link to="/personal-marketing">
                <Icon type="notification" />
                <span>Marketing</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="6">
              <Link to="/personal-source">
                <Icon type="project" />
                <span>Source</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="7">
              <Link to="/personal-documents">
                <Icon type="file" />
                <span>Documents</span>
              </Link>
            </Menu.Item>
            <SubMenu
              key="sub4"
              title={
                <span>
                  <Icon type="setting" />
                  <span>Settings</span>
                </span>
              }
            >
              <Menu.Item key="8">My Account</Menu.Item>
              <Menu.Item key="9">Company Settings</Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Layout>
          <Header className="mi-header" style={{ padding: 0 }}>
            <div className="header-block">
              <span className="header-trigger">
                <Icon
                  className="trigger"
                  type={this.state.collapsed ? "menu-unfold" : "menu-fold"}
                  onClick={this.toggle}
                />
              </span>
              <ul className="nav-actions">
                <li>
                  <Dropdown
                    overlay={<NotificationTab />}
                    placement="bottomRight"
                    trigger={["click"]}
                    className="dropdown-notification"
                  >
                    <a className="ant-dropdown-link d-dropdown-link" href="#">
                      <Icon type="bell" />
                    </a>
                  </Dropdown>
                </li>
                <li>
                  <Dropdown
                    overlay={userMenu}
                    placement="bottomRight"
                    trigger={["click"]}
                  >
                    <a className="ant-dropdown-link d-dropdown-link" href="#">
                      <Icon type="user" />
                    </a>
                  </Dropdown>
                </li>
                <li>
                  <Dropdown
                    overlay={messageMenu}
                    placement="bottomRight"
                    trigger={["click"]}
                  >
                    <a className="ant-dropdown-link d-dropdown-link" href="#">
                      <Icon type="message" />
                    </a>
                  </Dropdown>
                </li>
              </ul>
            </div>
          </Header>
          <Content
            style={{
              padding: 40,
              minHeight: 280
            }}
          >
            <AppRouter />
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default PageLayout;
