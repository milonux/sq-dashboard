import React, { Component } from "react";
import {Link} from "react-router-dom";
import { Form, Icon, Input, Button, Checkbox } from "antd";

const FormItem = Form.Item;

class LoginForm extends Component {
  state = {
    userName: "sq",
    password: "123456"
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (
        !err &&
        values.userName === this.state.userName &&
        values.password === this.state.password
      ) {
        this.props.history.push("/workspace");
      } else {
        this.setState({
          msg: "Username & Password not correct"
        });
      }
    });
  };
  render() {
    localStorage.clear();
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="login-form-container">
        <div className="header-login">
          <div className="login-logo">
            <img src="/assets/images/logo-white.png" alt="SQ"/>
          </div>
        </div>
        <div className="login-row">
          <div className="login-cell cell-login-hero" style={{backgroundImage: 'url(/assets/images/login-bg.jpg)'}}>
            <div className="login-cell-block">
              <div className="login-cell-content">
                <div className="login-hero-content">
                  <h1 className="login-heading">Welcome to SQ</h1>
                  <div className="login-subheading">
                    <p>
                    This is the new face of SQ. New look, new ideas, same core 
                    </p>
                  </div>
                  <div className="website-link">
                    <Link to='http://webmamu.com/static/qcn/'>www.sqgc.com/</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="login-cell cell-login-form">
            <div className="bg-image-mobile" style={{backgroundImage: 'url(/assets/images/login-bg.jpg)'}}></div>
            <div className="login-cell-block">
              <div className="login-logo text-center">
                <img src="/assets/images/logo-white.png" alt="SQ"/>
              </div>
              <div className="logo-with-form login-cell-content">
                <h4 className="mb-3">Login with SQ</h4>
                <Form onSubmit={this.handleSubmit} className="login-form">
                  <FormItem>
                    {getFieldDecorator("userName", {
                      rules: [
                        {
                          required: true,
                          message: "Please input your Username!"
                        }
                      ]
                    })(
                      <Input
                        size="large"
                        prefix={
                          <Icon
                            type="user"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                        placeholder="Username"
                      />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator("password", {
                      rules: [
                        {
                          required: true,
                          message: "Please input your Password!"
                        }
                      ]
                    })(
                      <Input
                        size="large"
                        prefix={
                          <Icon
                            type="lock"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                        type="password"
                        placeholder="Password"
                      />
                    )}
                  </FormItem>
                  <FormItem>
                    <ul className="login-meta">
                      <li>
                        <Link to='/forgot-password'>
                          Forgot password ?
                        </Link>
                      </li>
                      <li>
                        <Checkbox>Remember Me</Checkbox>
                      </li>
                    </ul>
                  </FormItem>
                  <FormItem>
                    <Button
                      type="primary"
                      htmlType="submit"
                      size="large"
                      className="login-form-button"
                      style={{ width: "100%" }}
                    >
                      Log in
                    </Button>
                  </FormItem>
                  <FormItem>
                    <p style={{ color: "red" }}>{this.state.msg}</p>
                  </FormItem>
                </Form>

                <div className="not-user">
                  New User? <Link to=''>Signup</Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


const WrappedLoginForm = Form.create()(LoginForm);

export default WrappedLoginForm;
