import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Login } from "./Auth";
import { Workspace } from "./Workspace";

import PersonalPageLayout from "./Personal/Layout/PageLayout";
import ManagementPageLayout from "./Management/Layout/PageLayout";

function App() {
  const panel = localStorage.panel;
  console.log("panel", panel);
  return (
    <Router>
      <Route path="/" exact component={Login} />
      <Route path="/workspace" exact component={Workspace} />
      {panel === "personal" && <PersonalPageLayout />}
      {panel === "management" && <ManagementPageLayout />}
    </Router>
  );
}

export default App;
